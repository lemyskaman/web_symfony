<?php

/* AppBundle:Home:index.html.twig */
class __TwigTemplate_ea8149bf20926d4fed517a0206241a4e53b99740ef22a4de7a2a2449e9526d57 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AppBundle::layout.html.twig", "AppBundle:Home:index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AppBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8db1bb54f55cd0173dea2bb311cd992c261daebc6d6424e5e0fb0353a5d84367 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8db1bb54f55cd0173dea2bb311cd992c261daebc6d6424e5e0fb0353a5d84367->enter($__internal_8db1bb54f55cd0173dea2bb311cd992c261daebc6d6424e5e0fb0353a5d84367_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Home:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8db1bb54f55cd0173dea2bb311cd992c261daebc6d6424e5e0fb0353a5d84367->leave($__internal_8db1bb54f55cd0173dea2bb311cd992c261daebc6d6424e5e0fb0353a5d84367_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_9c0dba4da0e8ff32c223ada31dbc6b60a0060ea310fc60ff1778e42e57799faa = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9c0dba4da0e8ff32c223ada31dbc6b60a0060ea310fc60ff1778e42e57799faa->enter($__internal_9c0dba4da0e8ff32c223ada31dbc6b60a0060ea310fc60ff1778e42e57799faa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "<div class=\"container-fluid\">
    <div class=\"row\">
        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"red\">
                    <i class=\"material-icons\">devices_other</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">Installs</p>
                     <h3 class=\"title\">";
        // line 12
        echo twig_escape_filter($this->env, ($context["devices_count"] ?? $this->getContext($context, "devices_count")), "html", null, true);
        echo "</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">perm_device_information</i><span> Application install</span> 
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"green\">
                    <i class=\"material-icons\">format_paint</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">Sets</p>
                     <h3 class=\"title\">";
        // line 28
        echo twig_escape_filter($this->env, ($context["count_sets"] ?? $this->getContext($context, "count_sets")), "html", null, true);
        echo "</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">keyboard_arrow_right</i><a href=\"";
        // line 32
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_wallpaper_index");
        echo "\">Wallpaper list</a>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"green\">
                    <i class=\"material-icons\">cloud_download</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">Downloads</p>
                     <h3 class=\"title\">";
        // line 44
        echo twig_escape_filter($this->env, ($context["count_downloads"] ?? $this->getContext($context, "count_downloads")), "html", null, true);
        echo "</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">keyboard_arrow_right</i><a href=\"";
        // line 48
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_wallpaper_index");
        echo "\">Wallpaper list</a>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"green\">
                    <i class=\"material-icons\">share</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">Shares</p>
                     <h3 class=\"title\">";
        // line 60
        echo twig_escape_filter($this->env, ($context["count_shares"] ?? $this->getContext($context, "count_shares")), "html", null, true);
        echo "</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">keyboard_arrow_right</i><a href=\"";
        // line 64
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_wallpaper_index");
        echo "\">Wallpaper list</a>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"green\">
                    <i class=\"material-icons\">remove_red_eye</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">Views</p>
                     <h3 class=\"title\">";
        // line 76
        echo twig_escape_filter($this->env, ($context["count_views"] ?? $this->getContext($context, "count_views")), "html", null, true);
        echo "</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">keyboard_arrow_right</i><a href=\"";
        // line 80
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_wallpaper_index");
        echo "\">Wallpaper list</a>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"rose\">
                    <i class=\"material-icons\">comment</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">Comments</p>
                     <h3 class=\"title\">";
        // line 92
        echo twig_escape_filter($this->env, ($context["comment_count"] ?? $this->getContext($context, "comment_count")), "html", null, true);
        echo "</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">keyboard_arrow_right</i><a href=\"";
        // line 96
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_comment_index");
        echo "\">Comments list</a>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"green\">
                    <i class=\"material-icons\">video_library</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">Videos</p>
                     <h3 class=\"title\">";
        // line 108
        echo twig_escape_filter($this->env, ($context["video_count"] ?? $this->getContext($context, "video_count")), "html", null, true);
        echo "</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">keyboard_arrow_right</i><a href=\"";
        // line 112
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_wallpaper_index");
        echo "\">Wallpaper list</a>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"green\">
                    <i class=\"material-icons\">gif</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">Gifs</p>
                     <h3 class=\"title\">";
        // line 124
        echo twig_escape_filter($this->env, ($context["gif_count"] ?? $this->getContext($context, "gif_count")), "html", null, true);
        echo "</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">keyboard_arrow_right</i><a href=\"";
        // line 128
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_wallpaper_index");
        echo "\">Wallpaper list</a>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"green\">
                    <i class=\"material-icons\">image</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">Images</p>
                     <h3 class=\"title\">";
        // line 140
        echo twig_escape_filter($this->env, ($context["image_count"] ?? $this->getContext($context, "image_count")), "html", null, true);
        echo "</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">keyboard_arrow_right</i><a href=\"";
        // line 144
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_wallpaper_index");
        echo "\">Wallpaper list</a>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"blue\">
                    <i class=\"material-icons\">access_time</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">Reviews</p>
                     <h3 class=\"title\">";
        // line 156
        echo twig_escape_filter($this->env, ($context["review_count"] ?? $this->getContext($context, "review_count")), "html", null, true);
        echo "</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">keyboard_arrow_right</i><a href=\"";
        // line 160
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_wallpaper_reviews");
        echo "\">Wallpaper to reviews</a>
                    </div>
                </div>
            </div>
        </div>


        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"red\">
                    <i class=\"material-icons\">view_list</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">Categoryies </p>
                     <h3 class=\"title\">";
        // line 174
        echo twig_escape_filter($this->env, ($context["category_count"] ?? $this->getContext($context, "category_count")), "html", null, true);
        echo "</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">keyboard_arrow_right</i><a href=\"";
        // line 178
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_category_index");
        echo "\">Categories list</a>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"red\">
                    <i class=\"material-icons\">inbox</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">Packs</p>
                     <h3 class=\"title\">";
        // line 190
        echo twig_escape_filter($this->env, ($context["pack_count"] ?? $this->getContext($context, "pack_count")), "html", null, true);
        echo "</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">keyboard_arrow_right</i><a href=\"";
        // line 194
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_pack_index");
        echo "\">packs list</a>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"red\">
                    <i class=\"material-icons\">palette</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">Colors</p>
                     <h3 class=\"title\">";
        // line 206
        echo twig_escape_filter($this->env, ($context["color_count"] ?? $this->getContext($context, "color_count")), "html", null, true);
        echo "</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">keyboard_arrow_right</i><a href=\"";
        // line 210
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_color_index");
        echo "\">Colors list</a>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"purple\">
                    <i class=\"material-icons\">group</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">users</p>
                     <h3 class=\"title\">";
        // line 222
        echo twig_escape_filter($this->env, ($context["users_count"] ?? $this->getContext($context, "users_count")), "html", null, true);
        echo "</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">keyboard_arrow_right</i><a href=\"";
        // line 226
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("user_user_index");
        echo "\">user list</a>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"rose\">
                    <i class=\"material-icons\">help</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">Supports</p>
                     <h3 class=\"title\">";
        // line 238
        echo twig_escape_filter($this->env, ($context["supports_count"] ?? $this->getContext($context, "supports_count")), "html", null, true);
        echo "</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">keyboard_arrow_right</i><a href=\"";
        // line 242
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_support_index");
        echo "\">Support messages list</a>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"black\">
                    <i class=\"material-icons\">info</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">Version</p>
                     <h3 class=\"title\">";
        // line 254
        echo twig_escape_filter($this->env, ($context["version_count"] ?? $this->getContext($context, "version_count")), "html", null, true);
        echo "</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">keyboard_arrow_right</i><a href=\"";
        // line 258
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_version_index");
        echo "\">Versions list</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
";
        
        $__internal_9c0dba4da0e8ff32c223ada31dbc6b60a0060ea310fc60ff1778e42e57799faa->leave($__internal_9c0dba4da0e8ff32c223ada31dbc6b60a0060ea310fc60ff1778e42e57799faa_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:Home:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  387 => 258,  380 => 254,  365 => 242,  358 => 238,  343 => 226,  336 => 222,  321 => 210,  314 => 206,  299 => 194,  292 => 190,  277 => 178,  270 => 174,  253 => 160,  246 => 156,  231 => 144,  224 => 140,  209 => 128,  202 => 124,  187 => 112,  180 => 108,  165 => 96,  158 => 92,  143 => 80,  136 => 76,  121 => 64,  114 => 60,  99 => 48,  92 => 44,  77 => 32,  70 => 28,  51 => 12,  40 => 3,  34 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"AppBundle::layout.html.twig\" %}
{% block body%}
<div class=\"container-fluid\">
    <div class=\"row\">
        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"red\">
                    <i class=\"material-icons\">devices_other</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">Installs</p>
                     <h3 class=\"title\">{{devices_count}}</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">perm_device_information</i><span> Application install</span> 
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"green\">
                    <i class=\"material-icons\">format_paint</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">Sets</p>
                     <h3 class=\"title\">{{count_sets}}</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">keyboard_arrow_right</i><a href=\"{{path(\"app_wallpaper_index\")}}\">Wallpaper list</a>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"green\">
                    <i class=\"material-icons\">cloud_download</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">Downloads</p>
                     <h3 class=\"title\">{{count_downloads}}</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">keyboard_arrow_right</i><a href=\"{{path(\"app_wallpaper_index\")}}\">Wallpaper list</a>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"green\">
                    <i class=\"material-icons\">share</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">Shares</p>
                     <h3 class=\"title\">{{count_shares}}</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">keyboard_arrow_right</i><a href=\"{{path(\"app_wallpaper_index\")}}\">Wallpaper list</a>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"green\">
                    <i class=\"material-icons\">remove_red_eye</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">Views</p>
                     <h3 class=\"title\">{{count_views}}</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">keyboard_arrow_right</i><a href=\"{{path(\"app_wallpaper_index\")}}\">Wallpaper list</a>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"rose\">
                    <i class=\"material-icons\">comment</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">Comments</p>
                     <h3 class=\"title\">{{comment_count}}</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">keyboard_arrow_right</i><a href=\"{{path(\"app_comment_index\")}}\">Comments list</a>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"green\">
                    <i class=\"material-icons\">video_library</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">Videos</p>
                     <h3 class=\"title\">{{video_count}}</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">keyboard_arrow_right</i><a href=\"{{path(\"app_wallpaper_index\")}}\">Wallpaper list</a>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"green\">
                    <i class=\"material-icons\">gif</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">Gifs</p>
                     <h3 class=\"title\">{{gif_count}}</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">keyboard_arrow_right</i><a href=\"{{path(\"app_wallpaper_index\")}}\">Wallpaper list</a>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"green\">
                    <i class=\"material-icons\">image</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">Images</p>
                     <h3 class=\"title\">{{image_count}}</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">keyboard_arrow_right</i><a href=\"{{path(\"app_wallpaper_index\")}}\">Wallpaper list</a>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"blue\">
                    <i class=\"material-icons\">access_time</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">Reviews</p>
                     <h3 class=\"title\">{{review_count}}</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">keyboard_arrow_right</i><a href=\"{{path(\"app_wallpaper_reviews\")}}\">Wallpaper to reviews</a>
                    </div>
                </div>
            </div>
        </div>


        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"red\">
                    <i class=\"material-icons\">view_list</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">Categoryies </p>
                     <h3 class=\"title\">{{category_count}}</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">keyboard_arrow_right</i><a href=\"{{path(\"app_category_index\")}}\">Categories list</a>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"red\">
                    <i class=\"material-icons\">inbox</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">Packs</p>
                     <h3 class=\"title\">{{pack_count}}</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">keyboard_arrow_right</i><a href=\"{{path(\"app_pack_index\")}}\">packs list</a>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"red\">
                    <i class=\"material-icons\">palette</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">Colors</p>
                     <h3 class=\"title\">{{color_count}}</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">keyboard_arrow_right</i><a href=\"{{path(\"app_color_index\")}}\">Colors list</a>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"purple\">
                    <i class=\"material-icons\">group</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">users</p>
                     <h3 class=\"title\">{{users_count}}</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">keyboard_arrow_right</i><a href=\"{{path(\"user_user_index\")}}\">user list</a>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"rose\">
                    <i class=\"material-icons\">help</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">Supports</p>
                     <h3 class=\"title\">{{supports_count}}</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">keyboard_arrow_right</i><a href=\"{{path(\"app_support_index\")}}\">Support messages list</a>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"black\">
                    <i class=\"material-icons\">info</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">Version</p>
                     <h3 class=\"title\">{{version_count}}</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">keyboard_arrow_right</i><a href=\"{{path(\"app_version_index\")}}\">Versions list</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{% endblock%}", "AppBundle:Home:index.html.twig", "/home/kaman/projects/Web/src/AppBundle/Resources/views/Home/index.html.twig");
    }
}
