<?php

/* AppBundle:Wallpaper:index.html.twig */
class __TwigTemplate_a25e09e68959d6f36f77e8a033307405dd54a1c6f0ab08ed6dd60998ececde4b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AppBundle::layout.html.twig", "AppBundle:Wallpaper:index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AppBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_783ebbc7494ab74e3a1e9a30d5c0fea3c2d7c3713afa76415b1858c8ab825b1d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_783ebbc7494ab74e3a1e9a30d5c0fea3c2d7c3713afa76415b1858c8ab825b1d->enter($__internal_783ebbc7494ab74e3a1e9a30d5c0fea3c2d7c3713afa76415b1858c8ab825b1d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Wallpaper:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_783ebbc7494ab74e3a1e9a30d5c0fea3c2d7c3713afa76415b1858c8ab825b1d->leave($__internal_783ebbc7494ab74e3a1e9a30d5c0fea3c2d7c3713afa76415b1858c8ab825b1d_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_ce4a526455eb0b035f0c4b3a8f11af143d5247a578c8c0cf88c9147485209933 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ce4a526455eb0b035f0c4b3a8f11af143d5247a578c8c0cf88c9147485209933->enter($__internal_ce4a526455eb0b035f0c4b3a8f11af143d5247a578c8c0cf88c9147485209933_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "\t<div class=\"container-fluid\">
\t\t<div class=\"row\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-4\" style=\"padding-right:10px;padding-left:10px;\">
\t\t\t\t\t<a href=\"";
        // line 7
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_wallpaper_index");
        echo "\" class=\"btn  btn-lg btn-warning col-md-12\"><i class=\"material-icons\" style=\"font-size: 30px;\">refresh</i> Refresh</a>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-4\" style=\"padding-right:10px;padding-left:10px;\">
\t\t\t\t\t<a class=\"btn btn btn-lg btn-yellow col-md-12\"><i class=\"material-icons\" style=\"font-size: 30px;\">wallpaper</i> ";
        // line 10
        echo twig_escape_filter($this->env, ($context["wallpaper_count"] ?? $this->getContext($context, "wallpaper_count")), "html", null, true);
        echo " Wallpapers</a>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-4\" style=\"padding-right:10px;padding-left:10px;\">
\t\t\t\t\t<div class=\"dropdown\">
\t\t\t\t\t\t<a href=\"#\" data-toggle=\"dropdown\" aria-expanded=\"false\" class=\"btn btn-rose btn-lg pull-right add-button col-md-12\"title=\"\"><i class=\"material-icons\" style=\"font-size: 30px;\">add_box</i> NEW WALLPAPER </a>
\t\t\t\t\t\t<ul class=\"dropdown-menu dropdown-menu-right\" role=\"menu\">
\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<a href=\"";
        // line 17
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_image_add");
        echo "\"><i class=\"material-icons\">image</i> UPLOAD WALLPAPER IMAGE</a>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<a href=\"";
        // line 20
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_gif_add");
        echo "\"><i class=\"material-icons\">gif</i> UPLOAD WALLPAPER GIF</a>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<a href=\"";
        // line 23
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_video_add");
        echo "\"><i class=\"material-icons\">cloud_upload</i> UPLOAD WALLPAPER VIDEO</a>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<a href=\"";
        // line 26
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_video_addurl");
        echo "\"><i class=\"material-icons\">link</i> ADD URL WALLPAPER VIDEO </a>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"row\">
\t\t\t\t";
        // line 33
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["wallpapers"] ?? $this->getContext($context, "wallpapers")));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["wallpaper"]) {
            // line 34
            echo "\t\t\t\t\t<div class=\"col-md-4 col-lg-3\" style=\"height:400px;padding-right:10px;padding-left:10px;\">
\t\t\t\t\t\t<div class=\"card card-product\"  >
\t\t\t\t\t\t\t<img  class=\"wallpaper-image\" src=\"";
            // line 36
            echo twig_escape_filter($this->env, $this->env->getExtension('Liip\ImagineBundle\Templating\ImagineExtension')->filter($this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($this->getAttribute($this->getAttribute($context["wallpaper"], "media", array()), "link", array())), "wallpaper_thumb"), "html", null, true);
            echo "\">
\t\t\t\t\t\t\t<div class=\"wallpaper-title\" >
\t\t\t\t\t\t\t\t";
            // line 38
            echo twig_escape_filter($this->env, $this->getAttribute($context["wallpaper"], "title", array()), "html", null, true);
            echo "
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"card-content\" style=\" padding: 0px 0px;\">
\t\t\t\t\t\t\t\t<div class=\"card-actions\">
\t\t\t\t\t\t\t\t\t<a href=\"";
            // line 42
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_wallpaper_view", array("id" => $this->getAttribute($context["wallpaper"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-info btn-simple btn-sm\" rel=\"tooltip\" data-placement=\"bottom\" title=\"\" data-original-title=\"View\">
\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\">remove_red_eye</i>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t";
            // line 45
            if (($this->getAttribute($context["wallpaper"], "type", array()) == "video")) {
                // line 46
                echo "\t\t\t\t\t\t\t\t\t\t";
                if (($this->getAttribute($this->getAttribute($context["wallpaper"], "video", array()), "enabled", array()) == true)) {
                    // line 47
                    echo "\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_video_edit", array("id" => $this->getAttribute($context["wallpaper"], "id", array()))), "html", null, true);
                    echo "\" class=\"btn btn-success btn-simple btn-sm\" rel=\"tooltip\" data-placement=\"bottom\" title=\"\" data-original-title=\"Edit\">
\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\">edit</i>
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 51
                    echo "\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_video_editurl", array("id" => $this->getAttribute($context["wallpaper"], "id", array()))), "html", null, true);
                    echo "\" class=\"btn btn-success btn-simple btn-sm\" rel=\"tooltip\" data-placement=\"bottom\" title=\"\" data-original-title=\"Edit\">
\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\">edit</i>
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t";
                }
                // line 55
                echo "\t\t\t\t\t\t\t\t\t";
            } elseif (($this->getAttribute($context["wallpaper"], "type", array()) == "gif")) {
                // line 56
                echo "\t\t\t\t\t\t\t\t\t<a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_gif_edit", array("id" => $this->getAttribute($context["wallpaper"], "id", array()))), "html", null, true);
                echo "\" class=\"btn btn-success btn-simple btn-sm\" rel=\"tooltip\" data-placement=\"bottom\" title=\"\" data-original-title=\"Edit\">
\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\">edit</i>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 60
                echo "\t\t\t\t\t\t\t\t\t<a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_image_edit", array("id" => $this->getAttribute($context["wallpaper"], "id", array()))), "html", null, true);
                echo "\" class=\"btn btn-success btn-simple btn-sm\" rel=\"tooltip\" data-placement=\"bottom\" title=\"\" data-original-title=\"Edit\">
\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\">edit</i>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t";
            }
            // line 64
            echo "\t\t\t\t\t\t\t\t\t<a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_home_notif_wallpaper", array("title" => $this->getAttribute($context["wallpaper"], "title", array()), "id" => $this->getAttribute($context["wallpaper"], "id", array()), "image" => $this->env->getExtension('Liip\ImagineBundle\Templating\ImagineExtension')->filter($this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($this->getAttribute($this->getAttribute($context["wallpaper"], "media", array()), "link", array())), "wallpaper_thumb"), "icon" => $this->env->getExtension('Liip\ImagineBundle\Templating\ImagineExtension')->filter($this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($this->getAttribute($this->getAttribute($context["wallpaper"], "media", array()), "link", array())), "wallpaper_thumb"))), "html", null, true);
            echo "\" class=\"btn btn-rose btn-simple btn-sm\" rel=\"tooltip\" data-placement=\"bottom\" title=\"\" data-original-title=\"Notification\">
\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\">notifications_active</i>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t<a href=\"";
            // line 67
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_wallpaper_delete", array("id" => $this->getAttribute($context["wallpaper"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-danger btn-simple btn-sm\" rel=\"tooltip\" data-placement=\"bottom\" title=\"\" data-original-title=\"Delete\">
\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\">close</i>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t";
            // line 70
            if (($this->getAttribute($context["wallpaper"], "type", array()) == "image")) {
                // line 71
                echo "\t\t\t\t\t\t\t\t\t\t<a class=\"pull-right\" style=\"background-color: #00000000 !important;color:white;\"><i class=\"material-icons\">image</i></a>
\t\t\t\t\t\t\t\t\t";
            } elseif (($this->getAttribute(            // line 72
$context["wallpaper"], "type", array()) == "gif")) {
                // line 73
                echo "\t\t\t\t\t\t\t\t\t\t<a class=\"pull-right\" style=\"background-color: #00000000 !important;color:white;\"><i class=\"material-icons\">gif</i></a>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 74
                echo "\t    
\t\t\t\t\t\t\t\t\t\t<a class=\"pull-right\" style=\"background-color: #00000000 !important;color:white;\"><i class=\"material-icons\">videocam</i></a>
\t\t\t\t\t\t\t\t\t";
            }
            // line 77
            echo "\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"card-footer\">
\t\t\t\t\t\t\t\t<div class=\"price\">
\t\t\t\t\t\t\t\t\t<div class=\"wallpaper-logo\" >
\t\t\t\t\t\t\t\t\t\t";
            // line 82
            if (($this->getAttribute($this->getAttribute($context["wallpaper"], "user", array()), "image", array()) == "")) {
                // line 83
                echo "\t\t\t\t\t\t\t\t\t\t\t";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["wallpaper"], "user", array()), "name", array()), "html", null, true);
                echo "
\t\t\t\t\t\t\t\t\t\t";
            } else {
                // line 85
                echo "\t\t\t\t\t\t\t\t\t\t\t<img src=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["wallpaper"], "user", array()), "image", array()), "html", null, true);
                echo "\" class=\"avatar-img\" alt=\"\">
\t\t\t\t\t\t\t\t\t\t";
            }
            // line 87
            echo "\t\t\t\t\t\t\t\t\t\t<span>";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["wallpaper"], "user", array()), "name", array()), "html", null, true);
            echo "</span>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"stats pull-right\">
\t\t\t\t\t\t\t\t\t<div class=\"wallpaper-logo\" >";
            // line 91
            echo $this->env->getExtension('Knp\Bundle\TimeBundle\Twig\Extension\TimeExtension')->diff($this->getAttribute($context["wallpaper"], "created", array()));
            echo "</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 97
            echo "\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t<div class=\"card-content\">
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<center><img src=\"";
            // line 102
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/bg_empty.png"), "html", null, true);
            echo "\"  style=\"width: auto !important;\" =\"\"></center>
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['wallpaper'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 109
        echo "\t\t\t\t
\t\t\t</div>
\t\t\t<div class=\" pull-right\">
\t\t\t\t";
        // line 112
        echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->render($this->env, ($context["wallpapers"] ?? $this->getContext($context, "wallpapers")));
        echo "
\t\t\t</div>
\t\t</div>
\t";
        
        $__internal_ce4a526455eb0b035f0c4b3a8f11af143d5247a578c8c0cf88c9147485209933->leave($__internal_ce4a526455eb0b035f0c4b3a8f11af143d5247a578c8c0cf88c9147485209933_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:Wallpaper:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  251 => 112,  246 => 109,  233 => 102,  226 => 97,  215 => 91,  207 => 87,  201 => 85,  195 => 83,  193 => 82,  186 => 77,  181 => 74,  177 => 73,  175 => 72,  172 => 71,  170 => 70,  164 => 67,  157 => 64,  149 => 60,  141 => 56,  138 => 55,  130 => 51,  122 => 47,  119 => 46,  117 => 45,  111 => 42,  104 => 38,  99 => 36,  95 => 34,  90 => 33,  80 => 26,  74 => 23,  68 => 20,  62 => 17,  52 => 10,  46 => 7,  40 => 3,  34 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"AppBundle::layout.html.twig\" %}
{% block body%}
\t<div class=\"container-fluid\">
\t\t<div class=\"row\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-4\" style=\"padding-right:10px;padding-left:10px;\">
\t\t\t\t\t<a href=\"{{path(\"app_wallpaper_index\")}}\" class=\"btn  btn-lg btn-warning col-md-12\"><i class=\"material-icons\" style=\"font-size: 30px;\">refresh</i> Refresh</a>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-4\" style=\"padding-right:10px;padding-left:10px;\">
\t\t\t\t\t<a class=\"btn btn btn-lg btn-yellow col-md-12\"><i class=\"material-icons\" style=\"font-size: 30px;\">wallpaper</i> {{wallpaper_count}} Wallpapers</a>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-4\" style=\"padding-right:10px;padding-left:10px;\">
\t\t\t\t\t<div class=\"dropdown\">
\t\t\t\t\t\t<a href=\"#\" data-toggle=\"dropdown\" aria-expanded=\"false\" class=\"btn btn-rose btn-lg pull-right add-button col-md-12\"title=\"\"><i class=\"material-icons\" style=\"font-size: 30px;\">add_box</i> NEW WALLPAPER </a>
\t\t\t\t\t\t<ul class=\"dropdown-menu dropdown-menu-right\" role=\"menu\">
\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<a href=\"{{path(\"app_image_add\")}}\"><i class=\"material-icons\">image</i> UPLOAD WALLPAPER IMAGE</a>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<a href=\"{{path(\"app_gif_add\")}}\"><i class=\"material-icons\">gif</i> UPLOAD WALLPAPER GIF</a>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<a href=\"{{path(\"app_video_add\")}}\"><i class=\"material-icons\">cloud_upload</i> UPLOAD WALLPAPER VIDEO</a>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<a href=\"{{path(\"app_video_addurl\")}}\"><i class=\"material-icons\">link</i> ADD URL WALLPAPER VIDEO </a>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"row\">
\t\t\t\t{% for wallpaper in wallpapers %}
\t\t\t\t\t<div class=\"col-md-4 col-lg-3\" style=\"height:400px;padding-right:10px;padding-left:10px;\">
\t\t\t\t\t\t<div class=\"card card-product\"  >
\t\t\t\t\t\t\t<img  class=\"wallpaper-image\" src=\"{{asset(wallpaper.media.link)|imagine_filter(\"wallpaper_thumb\")}}\">
\t\t\t\t\t\t\t<div class=\"wallpaper-title\" >
\t\t\t\t\t\t\t\t{{wallpaper.title}}
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"card-content\" style=\" padding: 0px 0px;\">
\t\t\t\t\t\t\t\t<div class=\"card-actions\">
\t\t\t\t\t\t\t\t\t<a href=\"{{path(\"app_wallpaper_view\",{id:wallpaper.id})}}\" class=\"btn btn-info btn-simple btn-sm\" rel=\"tooltip\" data-placement=\"bottom\" title=\"\" data-original-title=\"View\">
\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\">remove_red_eye</i>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t{% if wallpaper.type==\"video\" %}
\t\t\t\t\t\t\t\t\t\t{% if wallpaper.video.enabled == true  %}
\t\t\t\t\t\t\t\t\t\t\t<a href=\"{{path(\"app_video_edit\",{id:wallpaper.id})}}\" class=\"btn btn-success btn-simple btn-sm\" rel=\"tooltip\" data-placement=\"bottom\" title=\"\" data-original-title=\"Edit\">
\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\">edit</i>
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t{% else %}
\t\t\t\t\t\t\t\t\t\t\t<a href=\"{{path(\"app_video_editurl\",{id:wallpaper.id})}}\" class=\"btn btn-success btn-simple btn-sm\" rel=\"tooltip\" data-placement=\"bottom\" title=\"\" data-original-title=\"Edit\">
\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\">edit</i>
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t\t\t{% elseif wallpaper.type == \"gif\" %}
\t\t\t\t\t\t\t\t\t<a href=\"{{path(\"app_gif_edit\",{id:wallpaper.id})}}\" class=\"btn btn-success btn-simple btn-sm\" rel=\"tooltip\" data-placement=\"bottom\" title=\"\" data-original-title=\"Edit\">
\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\">edit</i>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t{% else %}
\t\t\t\t\t\t\t\t\t<a href=\"{{path(\"app_image_edit\",{id:wallpaper.id})}}\" class=\"btn btn-success btn-simple btn-sm\" rel=\"tooltip\" data-placement=\"bottom\" title=\"\" data-original-title=\"Edit\">
\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\">edit</i>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t\t\t<a href=\"{{path(\"app_home_notif_wallpaper\",{title:wallpaper.title,id:wallpaper.id,image:asset(wallpaper.media.link)|imagine_filter(\"wallpaper_thumb\"),icon:asset(wallpaper.media.link)|imagine_filter(\"wallpaper_thumb\")})}}\" class=\"btn btn-rose btn-simple btn-sm\" rel=\"tooltip\" data-placement=\"bottom\" title=\"\" data-original-title=\"Notification\">
\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\">notifications_active</i>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t<a href=\"{{path(\"app_wallpaper_delete\",{id:wallpaper.id})}}\" class=\"btn btn-danger btn-simple btn-sm\" rel=\"tooltip\" data-placement=\"bottom\" title=\"\" data-original-title=\"Delete\">
\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\">close</i>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t{% if wallpaper.type ==\"image\" %}
\t\t\t\t\t\t\t\t\t\t<a class=\"pull-right\" style=\"background-color: #00000000 !important;color:white;\"><i class=\"material-icons\">image</i></a>
\t\t\t\t\t\t\t\t\t{% elseif wallpaper.type == \"gif\" %}
\t\t\t\t\t\t\t\t\t\t<a class=\"pull-right\" style=\"background-color: #00000000 !important;color:white;\"><i class=\"material-icons\">gif</i></a>
\t\t\t\t\t\t\t\t\t{% else %}\t    
\t\t\t\t\t\t\t\t\t\t<a class=\"pull-right\" style=\"background-color: #00000000 !important;color:white;\"><i class=\"material-icons\">videocam</i></a>
\t\t\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"card-footer\">
\t\t\t\t\t\t\t\t<div class=\"price\">
\t\t\t\t\t\t\t\t\t<div class=\"wallpaper-logo\" >
\t\t\t\t\t\t\t\t\t\t{% if wallpaper.user.image == \"\" %}
\t\t\t\t\t\t\t\t\t\t\t{{wallpaper.user.name}}
\t\t\t\t\t\t\t\t\t\t{% else %}
\t\t\t\t\t\t\t\t\t\t\t<img src=\"{{wallpaper.user.image}}\" class=\"avatar-img\" alt=\"\">
\t\t\t\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t\t\t\t<span>{{wallpaper.user.name}}</span>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"stats pull-right\">
\t\t\t\t\t\t\t\t\t<div class=\"wallpaper-logo\" >{{wallpaper.created|ago}}</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t{% else %}
\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t<div class=\"card-content\">
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<center><img src=\"{{asset(\"img/bg_empty.png\")}}\"  style=\"width: auto !important;\" =\"\"></center>
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t{% endfor %}
\t\t\t\t
\t\t\t</div>
\t\t\t<div class=\" pull-right\">
\t\t\t\t{{ knp_pagination_render(wallpapers) }}
\t\t\t</div>
\t\t</div>
\t{% endblock%}", "AppBundle:Wallpaper:index.html.twig", "/home/kaman/projects/Web/src/AppBundle/Resources/views/Wallpaper/index.html.twig");
    }
}
