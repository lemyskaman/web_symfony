<?php

/* AppBundle::layout.html.twig */
class __TwigTemplate_46d6a87e72a8acd60aa91c77d7aa45ae1b17c8b4f0c5c5f9989e25735dbb3ead extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_db0c7031f91c3bcbdbfecec744b83a655f8f05ef20e4fb1aa8de63d8e31c3fbc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_db0c7031f91c3bcbdbfecec744b83a655f8f05ef20e4fb1aa8de63d8e31c3fbc->enter($__internal_db0c7031f91c3bcbdbfecec744b83a655f8f05ef20e4fb1aa8de63d8e31c3fbc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle::layout.html.twig"));

        // line 1
        echo "<!doctype html>
<html lang=\"en\">
<head>
    <meta charset=\"utf-8\" />
    <link rel=\"apple-touch-icon\" sizes=\"76x76\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/apple-icon.png"), "html", null, true);
        echo "\" />
    <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/favicon.png"), "html", null, true);
        echo "\" />
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\" />

    <title>Admin Panel | Wallpaper App </title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name=\"viewport\" content=\"width=device-width\" />

    <!-- Bootstrap core CSS     -->
    <link href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />

    <!--  Material Dashboard CSS    -->
    <link href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/material-dashboard.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/demo.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />

    <!--     Fonts and icons     -->
    <link href=\"https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css\" rel=\"stylesheet\">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
    <script src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jscolor.js"), "html", null, true);
        echo "\"></script>
    <link href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css\" rel=\"stylesheet\">
    <link href=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("lib/css/emoji.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("giflib/gifplayer.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">


    <link rel=\"stylesheet\" href=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("tags/css/normalize.css"), "html", null, true);
        echo "\">
    <!--[if IE 8]><script src=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/es5.js"), "html", null, true);
        echo "\"></script><![endif]-->
    <script src=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jquery.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("tags/js/standalone/selectize.js"), "html", null, true);
        echo "\"></script>
    <link rel=\"stylesheet\" href=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("tags/css/selectize.default.css"), "html", null, true);
        echo "\">
</head>

<body>
    ";
        // line 40
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "session", array()), "flashbag", array()), "all", array(), "method"));
        foreach ($context['_seq'] as $context["type"] => $context["messages"]) {
            // line 41
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["messages"]);
            foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                // line 42
                echo "            <div class=\"alert  alert-with-icon alert-dashborad\" data-notify=\"container\"  style=\"position: absolute;right: 20px;top: 0px;z-index: 1000;\">
                <i class=\"material-icons\" data-notify=\"icon\">notifications</i>
                <button type=\"button\" aria-hidden=\"true\" class=\"close\">
                    <i class=\"material-icons\">close</i>
                </button>
                <span data-notify=\"message\">";
                // line 47
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($context["message"]), "html", null, true);
                echo "</span>
            </div>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 50
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['type'], $context['messages'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 51
        echo "
    <div class=\"wrapper\">
        <div class=\"sidebar\" data-active-color=\"blue\"  data-background-color=\"white\" >
              <!--
                  Tip 1: You can change the color of the sidebar using: data-color=\"purple | blue | green | orange | red\"

                  Tip 2: you can also add an image using data-image tag
              -->
              <div class=\"logo\">
                  <img src=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/admin.png"), "html", null, true);
        echo "\" style=\"height: 100px;width: 100px;\">
                  <a href=\"";
        // line 61
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_home_index");
        echo "\" class=\"simple-text\">

                      WALLPAPER APP ADMIN
                  </a>
              </div>
              <div class=\"sidebar-wrapper\" style=\"overflow: scroll\">
                     <ul class=\"nav\">
                        <li ";
        // line 68
        if (twig_in_filter("app_home_index", $this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method"))) {
            echo " class=\"active\"";
        }
        echo ">
                            <a href=\"";
        // line 69
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_home_index");
        echo "\">
                                <i class=\"material-icons\">dashboard</i>
                                <p>Dashboard</p>
                            </a>
                        </li>
                        <li ";
        // line 74
        if (twig_in_filter("app_home_notif_", $this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method"))) {
            echo " class=\"active\"  aria-expanded=\"true\" ";
        }
        echo ">
                            <a data-toggle=\"collapse\" href=\"#notification\" class=\"\" >
                                <i class=\"material-icons\">notifications_active</i>
                                <p>Notifications
                                    <b class=\"caret\"></b>
                                </p>
                            </a>
                            <div class=\"collapse ";
        // line 81
        if (twig_in_filter("app_home_notif_", $this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method"))) {
            echo "  in ";
        }
        echo "\" id=\"notification\" aria-expanded=\"true\" style=\"\">
                                <ul class=\"nav\">
                                    <li  ";
        // line 83
        if (twig_in_filter("app_home_notif_wallpaper", $this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method"))) {
            echo " class=\"active\" ";
        }
        echo "><a href=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_home_notif_wallpaper");
        echo "\">Wallpaper</a></li>
                                    <li  ";
        // line 84
        if (twig_in_filter("app_home_notif_category", $this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method"))) {
            echo " class=\"active\" ";
        }
        echo "><a href=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_home_notif_category");
        echo "\">Category</a></li>
                                    <li  ";
        // line 85
        if (twig_in_filter("app_home_notif_url", $this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method"))) {
            echo " class=\"active\" ";
        }
        echo "><a href=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_home_notif_url");
        echo "\">Url</a></li>
                                </ul>
                            </div>
                        </li>
                        <li ";
        // line 89
        if (twig_in_filter("app_category_", $this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method"))) {
            echo " class=\"active\"";
        }
        echo ">
                            <a href=\"";
        // line 90
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_category_index");
        echo "\">
                                <i class=\"material-icons\">view_list</i>
                                <p>Categories</p>
                            </a>
                        </li>
                        <li ";
        // line 95
        if (twig_in_filter("color", $this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method"))) {
            echo " class=\"active\"";
        }
        echo "  >
                            <a href=\"";
        // line 96
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_color_index");
        echo "\">
                            <i class=\"material-icons\">palette</i>
                                <p>Colors</p>
                            </a>
                        </li> 
                        <li ";
        // line 101
        if (twig_in_filter("pack", $this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method"))) {
            echo " class=\"active\"";
        }
        echo "  >
                            <a href=\"";
        // line 102
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_pack_index");
        echo "\">
                            <i class=\"material-icons\">inbox</i>
                                <p>Packs</p>
                            </a>
                        </li> 
                        <li ";
        // line 107
        if (((((twig_in_filter("app_wallpaper_index", $this->getAttribute($this->getAttribute($this->getAttribute(        // line 108
($context["app"] ?? $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method")) || twig_in_filter("app_image", $this->getAttribute($this->getAttribute($this->getAttribute(        // line 109
($context["app"] ?? $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method"))) || twig_in_filter("app_video", $this->getAttribute($this->getAttribute($this->getAttribute(        // line 110
($context["app"] ?? $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method"))) || twig_in_filter("app_gif", $this->getAttribute($this->getAttribute($this->getAttribute(        // line 111
($context["app"] ?? $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method"))) || twig_in_filter("app_quote", $this->getAttribute($this->getAttribute($this->getAttribute(        // line 112
($context["app"] ?? $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method")))) {
            // line 113
            echo " class=\"active\"";
        }
        echo ">
                            <a href=\"";
        // line 114
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_wallpaper_index");
        echo "\">
                                <i class=\"material-icons\">wallpaper</i>
                                <p>Wallpapers</p>
                            </a>
                        </li>
                        <li ";
        // line 119
        if (twig_in_filter("app_slide_", $this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method"))) {
            echo " class=\"active\"";
        }
        echo ">
                            <a href=\"";
        // line 120
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_slide_index");
        echo "\">
                                <i class=\"material-icons\">slideshow</i>
                                <p>Slide</p>
                            </a>
                        </li>
                        <li ";
        // line 125
        if (twig_in_filter("app_wallpaper_review", $this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method"))) {
            echo " class=\"active\"";
        }
        echo ">
                            <a href=\"";
        // line 126
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_wallpaper_reviews");
        echo "\">
                                <i class=\"material-icons\">playlist_add_check</i>
                                <p>To review</p>
                            </a>
                        </li>
  
                             
                        <li ";
        // line 133
        if (twig_in_filter("comment", $this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method"))) {
            echo " class=\"active\"";
        }
        echo "  >
                            <a href=\"";
        // line 134
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_comment_index");
        echo "\">
                            <i class=\"material-icons\">comments</i>
                                <p>Comments</p>
                            </a>
                        </li>
                        <li ";
        // line 139
        if (twig_in_filter("tags", $this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method"))) {
            echo " class=\"active\"";
        }
        echo "  >
                            <a href=\"";
        // line 140
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_home_tags_index");
        echo "\">
                            <i class=\"material-icons\">label</i>
                                <p>Tags</p>
                            </a>
                        </li>   
                        <li ";
        // line 145
        if (twig_in_filter("support", $this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method"))) {
            echo " class=\"active\"";
        }
        echo "  >
                            <a href=\"";
        // line 146
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_support_index");
        echo "\">
                            <i class=\"material-icons\">messages</i>
                                <p>Support messages</p>
                            </a>
                        </li>
                        <li ";
        // line 151
        if (twig_in_filter("version", $this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method"))) {
            echo " class=\"active\"";
        }
        echo "  >
                            <a href=\"";
        // line 152
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_version_index");
        echo "\">
                            <i class=\"material-icons\">apps</i>
                                <p>Versions App</p>
                            </a>
                        </li>
                        <li ";
        // line 157
        if (twig_in_filter("user", $this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method"))) {
            echo " class=\"active\"";
        }
        echo "  >
                            <a href=\"";
        // line 158
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("user_user_index");
        echo "\">
                            <i class=\"material-icons\">group</i><p>Users</p>
                            </a>
                        </li>
                        <li>
                            <a href=\"";
        // line 163
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_change_password");
        echo "\">
                            <i class=\"material-icons\">lock</i><p>Change Password</p>
                            </a>
                        </li>
                        <li>
                            <a href=\"";
        // line 168
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_home_settings");
        echo "\">
                            <i class=\"material-icons\">settings</i><p>Settings</p>
                            </a>
                        </li>
                        <li>
                            <a href=\"";
        // line 173
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_logout");
        echo "\">
                            <i class=\"material-icons\">exit_to_app</i><p>Logout</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        <div class=\"main-panel\" style=\"overflow: scroll\">
            <nav class=\"navbar navbar-transparent navbar-absolute\">
                <div class=\"container-fluid\">
                    <div class=\"navbar-header\">
                        <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\">
                            <span class=\"sr-only\">Toggle navigation</span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                        </button>
                        <a class=\"navbar-brand\" href=\"#\">Dashboard</a>
                    </div>
                    <div class=\"collapse navbar-collapse\">

                    </div>
                </div>
            </nav>

            <div class=\"content\">
                ";
        // line 199
        $this->displayBlock('body', $context, $blocks);
        // line 202
        echo "            </div>

            <footer class=\"footer\">
            </footer>
        </div>
    </div>

</body>

    <!--   Core JS Files   -->
    <script src=\"";
        // line 212
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jquery-3.1.0.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
    <script src=\"";
        // line 213
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bootstrap.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
    <script src=\"";
        // line 214
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/material.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>

    <!--  Notifications Plugin    -->
    <script src=\"";
        // line 217
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bootstrap-notify.js"), "html", null, true);
        echo "\"></script>

    <!--  Google Maps Plugin    -->
    <script type=\"text/javascript\" src=\"https://maps.googleapis.com/maps/api/js\"></script>

    <!-- Material Dashboard javascript methods -->
    <script src=\"";
        // line 223
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/material-dashboard.js"), "html", null, true);
        echo "\"></script>

    <!-- Material Dashboard DEMO methods, don't include it in your project! -->
    <script src=\"";
        // line 226
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/demo.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 227
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/app.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 228
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("lib/js/config.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 229
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("lib/js/util.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 230
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("lib/js/jquery.emojiarea.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 231
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("lib/js/emoji-picker.js"), "html", null, true);
        echo "\"></script>


    <script>

      \$(function() {
        // Initializes and creates emoji set from sprite sheet
        window.emojiPicker = new EmojiPicker({
          emojiable_selector: '[data-emojiable=true]',
          assetsPath: '../lib/img/',
          popupButtonClasses: 'fa fa-smile-o'
        });
        // Finds all elements with `emojiable_selector` and converts them to rich emoji input fields
        // You may want to delay this step if you have dynamically created input fields that appear later in the loading process
        // It can be called as many times as necessary; previously converted input fields will not be converted again
        window.emojiPicker.discover();
      });
        function setTextColor(picker) {
            document.getElementsByTagName('body')[0].style.color = '#' + picker.toString()
        }
    </script>

</html>";
        
        $__internal_db0c7031f91c3bcbdbfecec744b83a655f8f05ef20e4fb1aa8de63d8e31c3fbc->leave($__internal_db0c7031f91c3bcbdbfecec744b83a655f8f05ef20e4fb1aa8de63d8e31c3fbc_prof);

    }

    // line 199
    public function block_body($context, array $blocks = array())
    {
        $__internal_84adefb6d9fc2e7fa2e5557ff9aa067462643636e298f37b7082bbfd1d94ba90 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_84adefb6d9fc2e7fa2e5557ff9aa067462643636e298f37b7082bbfd1d94ba90->enter($__internal_84adefb6d9fc2e7fa2e5557ff9aa067462643636e298f37b7082bbfd1d94ba90_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 200
        echo "                    
                ";
        
        $__internal_84adefb6d9fc2e7fa2e5557ff9aa067462643636e298f37b7082bbfd1d94ba90->leave($__internal_84adefb6d9fc2e7fa2e5557ff9aa067462643636e298f37b7082bbfd1d94ba90_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  525 => 200,  519 => 199,  489 => 231,  485 => 230,  481 => 229,  477 => 228,  473 => 227,  469 => 226,  463 => 223,  454 => 217,  448 => 214,  444 => 213,  440 => 212,  428 => 202,  426 => 199,  397 => 173,  389 => 168,  381 => 163,  373 => 158,  367 => 157,  359 => 152,  353 => 151,  345 => 146,  339 => 145,  331 => 140,  325 => 139,  317 => 134,  311 => 133,  301 => 126,  295 => 125,  287 => 120,  281 => 119,  273 => 114,  268 => 113,  266 => 112,  265 => 111,  264 => 110,  263 => 109,  262 => 108,  261 => 107,  253 => 102,  247 => 101,  239 => 96,  233 => 95,  225 => 90,  219 => 89,  208 => 85,  200 => 84,  192 => 83,  185 => 81,  173 => 74,  165 => 69,  159 => 68,  149 => 61,  145 => 60,  134 => 51,  128 => 50,  119 => 47,  112 => 42,  107 => 41,  103 => 40,  96 => 36,  92 => 35,  88 => 34,  84 => 33,  80 => 32,  74 => 29,  70 => 28,  65 => 26,  57 => 21,  51 => 18,  45 => 15,  33 => 6,  29 => 5,  23 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!doctype html>
<html lang=\"en\">
<head>
    <meta charset=\"utf-8\" />
    <link rel=\"apple-touch-icon\" sizes=\"76x76\" href=\"{{asset(\"img/apple-icon.png\")}}\" />
    <link rel=\"icon\" type=\"image/png\" href=\"{{asset(\"img/favicon.png\")}}\" />
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\" />

    <title>Admin Panel | Wallpaper App </title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name=\"viewport\" content=\"width=device-width\" />

    <!-- Bootstrap core CSS     -->
    <link href=\"{{asset(\"css/bootstrap.min.css\")}}\" rel=\"stylesheet\" />

    <!--  Material Dashboard CSS    -->
    <link href=\"{{asset(\"css/material-dashboard.css\")}}\" rel=\"stylesheet\"/>

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href=\"{{asset(\"css/demo.css\")}}\" rel=\"stylesheet\" />

    <!--     Fonts and icons     -->
    <link href=\"https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css\" rel=\"stylesheet\">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
    <script src=\"{{asset(\"js/jscolor.js\")}}\"></script>
    <link href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css\" rel=\"stylesheet\">
    <link href=\"{{asset(\"lib/css/emoji.css\")}}\" rel=\"stylesheet\">
    <link href=\"{{asset(\"giflib/gifplayer.css\")}}\" rel=\"stylesheet\">


    <link rel=\"stylesheet\" href=\"{{asset(\"tags/css/normalize.css\")}}\">
    <!--[if IE 8]><script src=\"{{asset(\"js/es5.js\")}}\"></script><![endif]-->
    <script src=\"{{asset(\"js/jquery.min.js\")}}\"></script>
    <script src=\"{{asset(\"tags/js/standalone/selectize.js\")}}\"></script>
    <link rel=\"stylesheet\" href=\"{{asset(\"tags/css/selectize.default.css\")}}\">
</head>

<body>
    {% for type, messages in app.session.flashbag.all() %}
        {% for message in messages %}
            <div class=\"alert  alert-with-icon alert-dashborad\" data-notify=\"container\"  style=\"position: absolute;right: 20px;top: 0px;z-index: 1000;\">
                <i class=\"material-icons\" data-notify=\"icon\">notifications</i>
                <button type=\"button\" aria-hidden=\"true\" class=\"close\">
                    <i class=\"material-icons\">close</i>
                </button>
                <span data-notify=\"message\">{{message|trans}}</span>
            </div>
        {% endfor %}
    {% endfor %}

    <div class=\"wrapper\">
        <div class=\"sidebar\" data-active-color=\"blue\"  data-background-color=\"white\" >
              <!--
                  Tip 1: You can change the color of the sidebar using: data-color=\"purple | blue | green | orange | red\"

                  Tip 2: you can also add an image using data-image tag
              -->
              <div class=\"logo\">
                  <img src=\"{{asset(\"img/admin.png\")}}\" style=\"height: 100px;width: 100px;\">
                  <a href=\"{{path(\"app_home_index\")}}\" class=\"simple-text\">

                      WALLPAPER APP ADMIN
                  </a>
              </div>
              <div class=\"sidebar-wrapper\" style=\"overflow: scroll\">
                     <ul class=\"nav\">
                        <li {% if \"app_home_index\" in  app.request.attributes.get('_route') %} class=\"active\"{% endif %}>
                            <a href=\"{{path(\"app_home_index\")}}\">
                                <i class=\"material-icons\">dashboard</i>
                                <p>Dashboard</p>
                            </a>
                        </li>
                        <li {% if \"app_home_notif_\" in  app.request.attributes.get('_route') %} class=\"active\"  aria-expanded=\"true\" {% endif %}>
                            <a data-toggle=\"collapse\" href=\"#notification\" class=\"\" >
                                <i class=\"material-icons\">notifications_active</i>
                                <p>Notifications
                                    <b class=\"caret\"></b>
                                </p>
                            </a>
                            <div class=\"collapse {% if \"app_home_notif_\" in  app.request.attributes.get('_route') %}  in {% endif %}\" id=\"notification\" aria-expanded=\"true\" style=\"\">
                                <ul class=\"nav\">
                                    <li  {% if \"app_home_notif_wallpaper\" in  app.request.attributes.get('_route') %} class=\"active\" {% endif %}><a href=\"{{path(\"app_home_notif_wallpaper\")}}\">Wallpaper</a></li>
                                    <li  {% if \"app_home_notif_category\" in  app.request.attributes.get('_route') %} class=\"active\" {% endif %}><a href=\"{{path(\"app_home_notif_category\")}}\">Category</a></li>
                                    <li  {% if \"app_home_notif_url\" in  app.request.attributes.get('_route') %} class=\"active\" {% endif %}><a href=\"{{path(\"app_home_notif_url\")}}\">Url</a></li>
                                </ul>
                            </div>
                        </li>
                        <li {% if \"app_category_\" in  app.request.attributes.get('_route')  %} class=\"active\"{% endif %}>
                            <a href=\"{{path(\"app_category_index\")}}\">
                                <i class=\"material-icons\">view_list</i>
                                <p>Categories</p>
                            </a>
                        </li>
                        <li {% if \"color\" in  app.request.attributes.get('_route') %} class=\"active\"{% endif %}  >
                            <a href=\"{{path(\"app_color_index\")}}\">
                            <i class=\"material-icons\">palette</i>
                                <p>Colors</p>
                            </a>
                        </li> 
                        <li {% if \"pack\" in  app.request.attributes.get('_route') %} class=\"active\"{% endif %}  >
                            <a href=\"{{path(\"app_pack_index\")}}\">
                            <i class=\"material-icons\">inbox</i>
                                <p>Packs</p>
                            </a>
                        </li> 
                        <li {% if 
                            \"app_wallpaper_index\" in  app.request.attributes.get('_route')
                            or \"app_image\" in  app.request.attributes.get('_route')
                            or \"app_video\" in  app.request.attributes.get('_route')
                            or \"app_gif\" in  app.request.attributes.get('_route')
                            or \"app_quote\" in  app.request.attributes.get('_route')
                          %} class=\"active\"{% endif %}>
                            <a href=\"{{path(\"app_wallpaper_index\")}}\">
                                <i class=\"material-icons\">wallpaper</i>
                                <p>Wallpapers</p>
                            </a>
                        </li>
                        <li {% if \"app_slide_\" in  app.request.attributes.get('_route')  %} class=\"active\"{% endif %}>
                            <a href=\"{{path(\"app_slide_index\")}}\">
                                <i class=\"material-icons\">slideshow</i>
                                <p>Slide</p>
                            </a>
                        </li>
                        <li {% if \"app_wallpaper_review\" in  app.request.attributes.get('_route')  %} class=\"active\"{% endif %}>
                            <a href=\"{{path(\"app_wallpaper_reviews\")}}\">
                                <i class=\"material-icons\">playlist_add_check</i>
                                <p>To review</p>
                            </a>
                        </li>
  
                             
                        <li {% if \"comment\" in  app.request.attributes.get('_route') %} class=\"active\"{% endif %}  >
                            <a href=\"{{path(\"app_comment_index\")}}\">
                            <i class=\"material-icons\">comments</i>
                                <p>Comments</p>
                            </a>
                        </li>
                        <li {% if \"tags\" in  app.request.attributes.get('_route') %} class=\"active\"{% endif %}  >
                            <a href=\"{{path(\"app_home_tags_index\")}}\">
                            <i class=\"material-icons\">label</i>
                                <p>Tags</p>
                            </a>
                        </li>   
                        <li {% if \"support\" in  app.request.attributes.get('_route') %} class=\"active\"{% endif %}  >
                            <a href=\"{{path(\"app_support_index\")}}\">
                            <i class=\"material-icons\">messages</i>
                                <p>Support messages</p>
                            </a>
                        </li>
                        <li {% if \"version\" in  app.request.attributes.get('_route') %} class=\"active\"{% endif %}  >
                            <a href=\"{{path(\"app_version_index\")}}\">
                            <i class=\"material-icons\">apps</i>
                                <p>Versions App</p>
                            </a>
                        </li>
                        <li {% if \"user\" in  app.request.attributes.get('_route') %} class=\"active\"{% endif %}  >
                            <a href=\"{{path(\"user_user_index\")}}\">
                            <i class=\"material-icons\">group</i><p>Users</p>
                            </a>
                        </li>
                        <li>
                            <a href=\"{{path(\"fos_user_change_password\")}}\">
                            <i class=\"material-icons\">lock</i><p>Change Password</p>
                            </a>
                        </li>
                        <li>
                            <a href=\"{{path(\"app_home_settings\")}}\">
                            <i class=\"material-icons\">settings</i><p>Settings</p>
                            </a>
                        </li>
                        <li>
                            <a href=\"{{path(\"fos_user_security_logout\")}}\">
                            <i class=\"material-icons\">exit_to_app</i><p>Logout</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        <div class=\"main-panel\" style=\"overflow: scroll\">
            <nav class=\"navbar navbar-transparent navbar-absolute\">
                <div class=\"container-fluid\">
                    <div class=\"navbar-header\">
                        <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\">
                            <span class=\"sr-only\">Toggle navigation</span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                        </button>
                        <a class=\"navbar-brand\" href=\"#\">Dashboard</a>
                    </div>
                    <div class=\"collapse navbar-collapse\">

                    </div>
                </div>
            </nav>

            <div class=\"content\">
                {% block body %}
                    
                {% endblock %}
            </div>

            <footer class=\"footer\">
            </footer>
        </div>
    </div>

</body>

    <!--   Core JS Files   -->
    <script src=\"{{asset(\"js/jquery-3.1.0.min.js\")}}\" type=\"text/javascript\"></script>
    <script src=\"{{asset(\"js/bootstrap.min.js\")}}\" type=\"text/javascript\"></script>
    <script src=\"{{asset(\"js/material.min.js\")}}\" type=\"text/javascript\"></script>

    <!--  Notifications Plugin    -->
    <script src=\"{{asset(\"js/bootstrap-notify.js\")}}\"></script>

    <!--  Google Maps Plugin    -->
    <script type=\"text/javascript\" src=\"https://maps.googleapis.com/maps/api/js\"></script>

    <!-- Material Dashboard javascript methods -->
    <script src=\"{{asset(\"js/material-dashboard.js\")}}\"></script>

    <!-- Material Dashboard DEMO methods, don't include it in your project! -->
    <script src=\"{{asset(\"js/demo.js\")}}\"></script>
    <script src=\"{{asset(\"js/app.js\")}}\"></script>
  <script src=\"{{asset(\"lib/js/config.js\")}}\"></script>
  <script src=\"{{asset(\"lib/js/util.js\")}}\"></script>
  <script src=\"{{asset(\"lib/js/jquery.emojiarea.js\")}}\"></script>
  <script src=\"{{asset(\"lib/js/emoji-picker.js\")}}\"></script>


    <script>

      \$(function() {
        // Initializes and creates emoji set from sprite sheet
        window.emojiPicker = new EmojiPicker({
          emojiable_selector: '[data-emojiable=true]',
          assetsPath: '../lib/img/',
          popupButtonClasses: 'fa fa-smile-o'
        });
        // Finds all elements with `emojiable_selector` and converts them to rich emoji input fields
        // You may want to delay this step if you have dynamically created input fields that appear later in the loading process
        // It can be called as many times as necessary; previously converted input fields will not be converted again
        window.emojiPicker.discover();
      });
        function setTextColor(picker) {
            document.getElementsByTagName('body')[0].style.color = '#' + picker.toString()
        }
    </script>

</html>", "AppBundle::layout.html.twig", "/home/kaman/projects/Web/src/AppBundle/Resources/views/layout.html.twig");
    }
}
