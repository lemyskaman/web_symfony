<?php

/* @WebProfiler/Profiler/ajax_layout.html.twig */
class __TwigTemplate_54a883f0904adad503660b2929329049a63030a3647c540d10f17faac0cc28b8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1f2df7d8d6b0f753df7f94b8b6792843f95ad9fd0f4aa51c5d54ccd0d9e73af0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1f2df7d8d6b0f753df7f94b8b6792843f95ad9fd0f4aa51c5d54ccd0d9e73af0->enter($__internal_1f2df7d8d6b0f753df7f94b8b6792843f95ad9fd0f4aa51c5d54ccd0d9e73af0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_1f2df7d8d6b0f753df7f94b8b6792843f95ad9fd0f4aa51c5d54ccd0d9e73af0->leave($__internal_1f2df7d8d6b0f753df7f94b8b6792843f95ad9fd0f4aa51c5d54ccd0d9e73af0_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_15974f1536b055a37d7a5f08a717b4c0a39411472a91a45025f3a33d7d180b8a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_15974f1536b055a37d7a5f08a717b4c0a39411472a91a45025f3a33d7d180b8a->enter($__internal_15974f1536b055a37d7a5f08a717b4c0a39411472a91a45025f3a33d7d180b8a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_15974f1536b055a37d7a5f08a717b4c0a39411472a91a45025f3a33d7d180b8a->leave($__internal_15974f1536b055a37d7a5f08a717b4c0a39411472a91a45025f3a33d7d180b8a_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% block panel '' %}
", "@WebProfiler/Profiler/ajax_layout.html.twig", "/home/kaman/projects/Web/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/ajax_layout.html.twig");
    }
}
