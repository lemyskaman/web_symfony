<?php

/* AppBundle:Wallpaper:image_add.html.twig */
class __TwigTemplate_29554101592e5467c32d9eca4f7d6bf3363c132402f2050dd0cce61cb8e2c14a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AppBundle::layout.html.twig", "AppBundle:Wallpaper:image_add.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AppBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0d6fa7c555fb3e0e7ef5f81660cf56b70f1f9dff71b753dba18d3b8d1547595a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0d6fa7c555fb3e0e7ef5f81660cf56b70f1f9dff71b753dba18d3b8d1547595a->enter($__internal_0d6fa7c555fb3e0e7ef5f81660cf56b70f1f9dff71b753dba18d3b8d1547595a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Wallpaper:image_add.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0d6fa7c555fb3e0e7ef5f81660cf56b70f1f9dff71b753dba18d3b8d1547595a->leave($__internal_0d6fa7c555fb3e0e7ef5f81660cf56b70f1f9dff71b753dba18d3b8d1547595a_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_5a158bcd45d162b2554f1ab22d2d210db5584a0cde15edb91429cd8d4bc5bba6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5a158bcd45d162b2554f1ab22d2d210db5584a0cde15edb91429cd8d4bc5bba6->enter($__internal_5a158bcd45d162b2554f1ab22d2d210db5584a0cde15edb91429cd8d4bc5bba6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "  <div class=\"container-fluid\">
    <div class=\"row\">
      <div class=\"col-sm-offset-1 col-md-10\">
        <div class=\"card\">
          <div class=\"card-header card-header-icon\" data-background-color=\"rose\">
            <i class=\"material-icons\">image</i>
          </div>
          <div class=\"card-content\">
            <h4 class=\"card-title\">New Wallpaper Image modified</h4>
            ";
        // line 12
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
            <br>
               <div class=\"form-group label-floating \">
                  <label class=\"control-label\">Wallpaper title</label>
                  ";
        // line 16
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "title", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                  <span class=\"validate-input\">";
        // line 17
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "title", array()), 'errors');
        echo "</span>
              </div>
              <div class=\"form-group label-floating \">
                  <label class=\"control-label\">Wallpaper description</label>
                  ";
        // line 21
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "description", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                  <span class=\"validate-input\">";
        // line 22
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "description", array()), 'errors');
        echo "</span>
              </div>
               <img src=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/image_placeholder.jpg"), "html", null, true);
        echo "\" class=\"fileinput-preview thumbnail \" id=\"img-preview\">
                <div class=\"form-group label-floating \">
                  <label class=\"control-label\">archivo</label>
                  ";
        // line 27
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "file", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                  <span class=\"validate-input\">";
        // line 28
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "title", array()), 'errors');
        echo "</span>
              </div>
                <!--<div class=\"fileinput fileinput-new text-center\" data-provides=\"fileinput\" style=\"width: 100%;\">
                    <div>
                        <a href=\"#\" class=\"btn btn-rose btn-round btn-select\" style=\"width: 100%;margin:0px;\"><i class=\"material-icons\">image</i> Select Image</a>
                    </div>
                    ";
        // line 34
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "file", array()), 'widget', array("attr" => array("class" => "file-hidden input-file img-selector", "style" => "   /* display: none; */height: 0px;width: 0px;position: absolute;")));
        echo "
                    <span class=\"validate-input\">";
        // line 35
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "file", array()), 'errors');
        echo "</span>
               </div>-->
               <br>
              <div class=\"\">
                    <label>
                      ";
        // line 40
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "premium", array()), 'widget');
        echo "  Premium wallpaper
                    </label>
              </div>
              <div class=\"\">
                    <label>
                      ";
        // line 45
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "enabled", array()), 'widget');
        echo "  Enabled
                    </label>
              </div>
              <div class=\"\">
                    <label>
                      ";
        // line 50
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "comment", array()), 'widget');
        echo "  Enabled Comment
                    </label>
              </div>
              <br>
              ";
        // line 54
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "categories", array()), 'label', array("label_attr" => array("style" => "font-size:16px")));
        echo " :
              <div>
                 ";
        // line 56
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "categories", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["field"]) {
            // line 57
            echo "                      <label   class=\"color-label\" >
                          <span class=\"color-label-checkbox\" >";
            // line 58
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($context["field"], 'widget');
            echo "</span ><span class=\"color-label-text\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["field"], "vars", array()), "label", array()), "html", null, true);
            echo "</span>
                      </label>
                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 61
        echo "              </div>
              <br>
              ";
        // line 63
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "colors", array()), 'label', array("label_attr" => array("style" => "font-size:16px")));
        echo " :
              <div>
                 ";
        // line 65
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "colors", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["field"]) {
            // line 66
            echo "                      <label   class=\"color-label\" style=\"background: #";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["colors"] ?? $this->getContext($context, "colors")));
            foreach ($context['_seq'] as $context["_key"] => $context["color"]) {
                if (($this->getAttribute($context["color"], "id", array()) == $this->getAttribute($this->getAttribute($context["field"], "vars", array()), "value", array()))) {
                    echo twig_escape_filter($this->env, $this->getAttribute($context["color"], "code", array()), "html", null, true);
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['color'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo "\">
                          <span class=\"color-label-checkbox\" style=\"background: #";
            // line 67
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["colors"] ?? $this->getContext($context, "colors")));
            foreach ($context['_seq'] as $context["_key"] => $context["color"]) {
                if (($this->getAttribute($context["color"], "id", array()) == $this->getAttribute($this->getAttribute($context["field"], "vars", array()), "value", array()))) {
                    echo twig_escape_filter($this->env, $this->getAttribute($context["color"], "code", array()), "html", null, true);
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['color'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo "\">";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($context["field"], 'widget');
            echo "</span ><span class=\"color-label-text\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["field"], "vars", array()), "label", array()), "html", null, true);
            echo "</span>
                      </label>
                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 70
        echo "              </div>
              <br>
              ";
        // line 72
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "packs", array()), 'label', array("label_attr" => array("style" => "font-size:16px")));
        echo " :
              <div>
                 ";
        // line 74
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "packs", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["field"]) {
            // line 75
            echo "                      <label   class=\"color-label\" >
                          <span class=\"color-label-checkbox\" >";
            // line 76
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($context["field"], 'widget');
            echo "</span ><span class=\"color-label-text\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["field"], "vars", array()), "label", array()), "html", null, true);
            echo "</span>
                      </label>
                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 79
        echo "              </div><br>
              <div class=\"form-group label-floating \">
                  <label class=\"control-label\">Wallpaper tags (Ex:anim,art,hero)</label>
                  <br>
                  ";
        // line 83
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "tags", array()), 'widget', array("attr" => array("class" => "input-tags")));
        echo "
                  <span class=\"validate-input\">";
        // line 84
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "tags", array()), 'errors');
        echo "</span>
              </div>
              <script>
              \$('.input-tags').selectize({
                persist: false,
                createOnBlur: true,
                create: true
              });
              </script>
            <br>
              <span class=\"pull-right\"><a href=\"";
        // line 94
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_wallpaper_index");
        echo "\" class=\"btn btn-fill btn-yellow\"><i class=\"material-icons\">arrow_back</i> Cancel</a>";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "save", array()), 'widget', array("attr" => array("class" => "btn btn-fill btn-rose")));
        echo "</span>
            ";
        // line 95
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
          </div>
        </div>
      </div>
    </div>
  </div>
";
        
        $__internal_5a158bcd45d162b2554f1ab22d2d210db5584a0cde15edb91429cd8d4bc5bba6->leave($__internal_5a158bcd45d162b2554f1ab22d2d210db5584a0cde15edb91429cd8d4bc5bba6_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:Wallpaper:image_add.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  260 => 95,  254 => 94,  241 => 84,  237 => 83,  231 => 79,  220 => 76,  217 => 75,  213 => 74,  208 => 72,  204 => 70,  182 => 67,  168 => 66,  164 => 65,  159 => 63,  155 => 61,  144 => 58,  141 => 57,  137 => 56,  132 => 54,  125 => 50,  117 => 45,  109 => 40,  101 => 35,  97 => 34,  88 => 28,  84 => 27,  78 => 24,  73 => 22,  69 => 21,  62 => 17,  58 => 16,  51 => 12,  40 => 3,  34 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"AppBundle::layout.html.twig\" %}
{% block body%}
  <div class=\"container-fluid\">
    <div class=\"row\">
      <div class=\"col-sm-offset-1 col-md-10\">
        <div class=\"card\">
          <div class=\"card-header card-header-icon\" data-background-color=\"rose\">
            <i class=\"material-icons\">image</i>
          </div>
          <div class=\"card-content\">
            <h4 class=\"card-title\">New Wallpaper Image modified</h4>
            {{form_start(form)}}
            <br>
               <div class=\"form-group label-floating \">
                  <label class=\"control-label\">Wallpaper title</label>
                  {{form_widget(form.title,{\"attr\":{\"class\":\"form-control\"}})}}
                  <span class=\"validate-input\">{{form_errors(form.title)}}</span>
              </div>
              <div class=\"form-group label-floating \">
                  <label class=\"control-label\">Wallpaper description</label>
                  {{form_widget(form.description,{\"attr\":{\"class\":\"form-control\"}})}}
                  <span class=\"validate-input\">{{form_errors(form.description)}}</span>
              </div>
               <img src=\"{{asset(\"img/image_placeholder.jpg\")}}\" class=\"fileinput-preview thumbnail \" id=\"img-preview\">
                <div class=\"form-group label-floating \">
                  <label class=\"control-label\">archivo</label>
                  {{form_widget(form.file,{\"attr\":{\"class\":\"form-control\"}})}}
                  <span class=\"validate-input\">{{form_errors(form.title)}}</span>
              </div>
                <!--<div class=\"fileinput fileinput-new text-center\" data-provides=\"fileinput\" style=\"width: 100%;\">
                    <div>
                        <a href=\"#\" class=\"btn btn-rose btn-round btn-select\" style=\"width: 100%;margin:0px;\"><i class=\"material-icons\">image</i> Select Image</a>
                    </div>
                    {{form_widget(form.file,{\"attr\":{\"class\":\"file-hidden input-file img-selector\",\"style\":\"   /* display: none; */height: 0px;width: 0px;position: absolute;\"}})}}
                    <span class=\"validate-input\">{{form_errors(form.file)}}</span>
               </div>-->
               <br>
              <div class=\"\">
                    <label>
                      {{form_widget(form.premium)}}  Premium wallpaper
                    </label>
              </div>
              <div class=\"\">
                    <label>
                      {{form_widget(form.enabled)}}  Enabled
                    </label>
              </div>
              <div class=\"\">
                    <label>
                      {{form_widget(form.comment)}}  Enabled Comment
                    </label>
              </div>
              <br>
              {{form_label(form.categories,null,{label_attr:{\"style\":\"font-size:16px\"}})}} :
              <div>
                 {% for field in form.categories %}
                      <label   class=\"color-label\" >
                          <span class=\"color-label-checkbox\" >{{ form_widget(field) }}</span ><span class=\"color-label-text\">{{ field.vars.label }}</span>
                      </label>
                  {% endfor %}
              </div>
              <br>
              {{form_label(form.colors,null,{label_attr:{\"style\":\"font-size:16px\"}})}} :
              <div>
                 {% for field in form.colors %}
                      <label   class=\"color-label\" style=\"background: #{% for color in colors %}{% if color.id == field.vars.value %}{{color.code}}{% endif %}{% endfor %}\">
                          <span class=\"color-label-checkbox\" style=\"background: #{% for color in colors %}{% if color.id == field.vars.value %}{{color.code}}{% endif %}{% endfor %}\">{{ form_widget(field) }}</span ><span class=\"color-label-text\">{{ field.vars.label }}</span>
                      </label>
                  {% endfor %}
              </div>
              <br>
              {{form_label(form.packs,null,{label_attr:{\"style\":\"font-size:16px\"}})}} :
              <div>
                 {% for field in form.packs %}
                      <label   class=\"color-label\" >
                          <span class=\"color-label-checkbox\" >{{ form_widget(field) }}</span ><span class=\"color-label-text\">{{ field.vars.label }}</span>
                      </label>
                  {% endfor %}
              </div><br>
              <div class=\"form-group label-floating \">
                  <label class=\"control-label\">Wallpaper tags (Ex:anim,art,hero)</label>
                  <br>
                  {{form_widget(form.tags,{\"attr\":{\"class\":\"input-tags\"}})}}
                  <span class=\"validate-input\">{{form_errors(form.tags)}}</span>
              </div>
              <script>
              \$('.input-tags').selectize({
                persist: false,
                createOnBlur: true,
                create: true
              });
              </script>
            <br>
              <span class=\"pull-right\"><a href=\"{{path(\"app_wallpaper_index\")}}\" class=\"btn btn-fill btn-yellow\"><i class=\"material-icons\">arrow_back</i> Cancel</a>{{form_widget(form.save,{attr:{\"class\":\"btn btn-fill btn-rose\"}})}}</span>
            {{form_end(form)}}
          </div>
        </div>
      </div>
    </div>
  </div>
{% endblock%}", "AppBundle:Wallpaper:image_add.html.twig", "/home/kaman/projects/Web/src/AppBundle/Resources/views/Wallpaper/image_add.html.twig");
    }
}
