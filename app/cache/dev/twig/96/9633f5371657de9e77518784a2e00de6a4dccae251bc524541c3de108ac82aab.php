<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_3d20cbae5d698c03293e82452b5ee7ab986f9971e0f32bf28dcfff6f7f20a1ee extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b4cd5e6e3f4637271c19db25e0fa4d93226f9af037e40d11278faca80f71bc48 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b4cd5e6e3f4637271c19db25e0fa4d93226f9af037e40d11278faca80f71bc48->enter($__internal_b4cd5e6e3f4637271c19db25e0fa4d93226f9af037e40d11278faca80f71bc48_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b4cd5e6e3f4637271c19db25e0fa4d93226f9af037e40d11278faca80f71bc48->leave($__internal_b4cd5e6e3f4637271c19db25e0fa4d93226f9af037e40d11278faca80f71bc48_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_7e2f9b772f451b292772afb17c89389801464b4ed454028119ae9b8ad85862be = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7e2f9b772f451b292772afb17c89389801464b4ed454028119ae9b8ad85862be->enter($__internal_7e2f9b772f451b292772afb17c89389801464b4ed454028119ae9b8ad85862be_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpFoundationExtension')->generateAbsoluteUrl($this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_7e2f9b772f451b292772afb17c89389801464b4ed454028119ae9b8ad85862be->leave($__internal_7e2f9b772f451b292772afb17c89389801464b4ed454028119ae9b8ad85862be_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_29047e9ebc2f5af18f01a1bac04e701fa5a3424b145cc12c7d80c86cca8f6246 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_29047e9ebc2f5af18f01a1bac04e701fa5a3424b145cc12c7d80c86cca8f6246->enter($__internal_29047e9ebc2f5af18f01a1bac04e701fa5a3424b145cc12c7d80c86cca8f6246_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_29047e9ebc2f5af18f01a1bac04e701fa5a3424b145cc12c7d80c86cca8f6246->leave($__internal_29047e9ebc2f5af18f01a1bac04e701fa5a3424b145cc12c7d80c86cca8f6246_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_983615c9fc36d95ea21b3b82592d84af6d8c0599bc381c9fe4c31abec0f5f855 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_983615c9fc36d95ea21b3b82592d84af6d8c0599bc381c9fe4c31abec0f5f855->enter($__internal_983615c9fc36d95ea21b3b82592d84af6d8c0599bc381c9fe4c31abec0f5f855_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_983615c9fc36d95ea21b3b82592d84af6d8c0599bc381c9fe4c31abec0f5f855->leave($__internal_983615c9fc36d95ea21b3b82592d84af6d8c0599bc381c9fe4c31abec0f5f855_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <link href=\"{{ absolute_url(asset('bundles/framework/css/exception.css')) }}\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "/home/kaman/projects/Web/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception_full.html.twig");
    }
}
