<?php

/* AppBundle:Wallpaper:video_add_url.html.twig */
class __TwigTemplate_58f19cf2f24feaa1b409c88e6db680e1a8c87f03f797821ef32e3a9939a6b28e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AppBundle::layout.html.twig", "AppBundle:Wallpaper:video_add_url.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AppBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_22e9f10b84d14b531de2f7a66e296d86fcbd017413b7ccfeb7c734b38dd48404 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_22e9f10b84d14b531de2f7a66e296d86fcbd017413b7ccfeb7c734b38dd48404->enter($__internal_22e9f10b84d14b531de2f7a66e296d86fcbd017413b7ccfeb7c734b38dd48404_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Wallpaper:video_add_url.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_22e9f10b84d14b531de2f7a66e296d86fcbd017413b7ccfeb7c734b38dd48404->leave($__internal_22e9f10b84d14b531de2f7a66e296d86fcbd017413b7ccfeb7c734b38dd48404_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_190ae36a385d3d7d37e275d138cf59925ce2449b89b6564f78cec79b1a8e07e5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_190ae36a385d3d7d37e275d138cf59925ce2449b89b6564f78cec79b1a8e07e5->enter($__internal_190ae36a385d3d7d37e275d138cf59925ce2449b89b6564f78cec79b1a8e07e5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "  <div class=\"container-fluid\">
    <div class=\"row\">
      <div class=\"col-sm-offset-1 col-md-10\">
        <div class=\"card\">
          <div class=\"card-header card-header-icon\" data-background-color=\"rose\">
            <i class=\"material-icons\">video_library</i>
          </div>
          <div class=\"card-content\">
            <h4 class=\"card-title\">New Wallpaper</h4>
            ";
        // line 12
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
               <div class=\"form-group label-floating \">
                  <label class=\"control-label\">Wallpaper title</label>
                  ";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "title", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                  <span class=\"validate-input\">";
        // line 16
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "title", array()), 'errors');
        echo "</span>
              </div>
              <div class=\"form-group label-floating \">
                  <label class=\"control-label\">Wallpaper description</label>
                  ";
        // line 20
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "description", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                  <span class=\"validate-input\">";
        // line 21
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "description", array()), 'errors');
        echo "</span>
              </div>
              <div class=\"form-group label-floating \">
                  <label class=\"control-label\">Wallpaper url</label>
                  ";
        // line 25
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "urlvideo", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                  <span class=\"validate-input\">";
        // line 26
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "urlvideo", array()), 'errors');
        echo "</span>
              </div>
               <img src=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/image_placeholder.jpg"), "html", null, true);
        echo "\" class=\"fileinput-preview thumbnail \" id=\"img-preview\">
                <div class=\"fileinput fileinput-new text-center\" data-provides=\"fileinput\" style=\"width: 100%;\">
                    <div>
                        <a href=\"#\" class=\"btn btn-rose btn-round btn-select\" style=\"width: 100%;margin:0px;\"><i class=\"material-icons\">image</i> Select Image</a>
                    </div>
                    ";
        // line 33
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "file", array()), 'widget', array("attr" => array("class" => "file-hidden input-file img-selector", "style" => "   /* display: none; */height: 0px;width: 0px;position: absolute;")));
        echo "
                    <span class=\"validate-input\">";
        // line 34
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "file", array()), 'errors');
        echo "</span>
               </div>
               
              <div class=\"\">
                    <label>
                      ";
        // line 39
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "premium", array()), 'widget');
        echo "  Premium wallpaper
                    </label>
              </div>
              <div class=\"\">
                    <label>
                      ";
        // line 44
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "enabled", array()), 'widget');
        echo "  Enabled
                    </label>
              </div>
              <div class=\"\">
                    <label>
                      ";
        // line 49
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "comment", array()), 'widget');
        echo "  Enabled Comment
                    </label>
              </div>
              <br>
              ";
        // line 53
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "categories", array()), 'label', array("label_attr" => array("style" => "font-size:16px")));
        echo " :
              <div>
                 ";
        // line 55
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "categories", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["field"]) {
            // line 56
            echo "                      <label   class=\"color-label\" >
                          <span class=\"color-label-checkbox\" >";
            // line 57
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($context["field"], 'widget');
            echo "</span ><span class=\"color-label-text\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["field"], "vars", array()), "label", array()), "html", null, true);
            echo "</span>
                      </label>
                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 60
        echo "              </div>
              <br>
              ";
        // line 62
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "colors", array()), 'label', array("label_attr" => array("style" => "font-size:16px")));
        echo " :
              <div>
                 ";
        // line 64
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "colors", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["field"]) {
            // line 65
            echo "                      <label   class=\"color-label\" style=\"background: #";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["colors"] ?? $this->getContext($context, "colors")));
            foreach ($context['_seq'] as $context["_key"] => $context["color"]) {
                if (($this->getAttribute($context["color"], "id", array()) == $this->getAttribute($this->getAttribute($context["field"], "vars", array()), "value", array()))) {
                    echo twig_escape_filter($this->env, $this->getAttribute($context["color"], "code", array()), "html", null, true);
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['color'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo "\">
                          <span class=\"color-label-checkbox\" style=\"background: #";
            // line 66
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["colors"] ?? $this->getContext($context, "colors")));
            foreach ($context['_seq'] as $context["_key"] => $context["color"]) {
                if (($this->getAttribute($context["color"], "id", array()) == $this->getAttribute($this->getAttribute($context["field"], "vars", array()), "value", array()))) {
                    echo twig_escape_filter($this->env, $this->getAttribute($context["color"], "code", array()), "html", null, true);
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['color'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo "\">";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($context["field"], 'widget');
            echo "</span ><span class=\"color-label-text\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["field"], "vars", array()), "label", array()), "html", null, true);
            echo "</span>
                      </label>
                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 69
        echo "              </div>
              <br>
              ";
        // line 71
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "packs", array()), 'label', array("label_attr" => array("style" => "font-size:16px")));
        echo " :
              <div>
                 ";
        // line 73
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "packs", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["field"]) {
            // line 74
            echo "                      <label   class=\"color-label\" >
                          <span class=\"color-label-checkbox\" >";
            // line 75
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($context["field"], 'widget');
            echo "</span ><span class=\"color-label-text\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["field"], "vars", array()), "label", array()), "html", null, true);
            echo "</span>
                      </label>
                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 78
        echo "              </div><br>
              <div class=\"form-group label-floating \">
                  <label class=\"control-label\">Wallpaper tags (Ex:anim,art,hero)</label>
                  <br>
                  ";
        // line 82
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "tags", array()), 'widget', array("attr" => array("class" => "input-tags")));
        echo "
                  <span class=\"validate-input\">";
        // line 83
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "tags", array()), 'errors');
        echo "</span>
              </div>
              <script>
              \$('.input-tags').selectize({
                persist: false,
                createOnBlur: true,
                create: true
              });
              </script>
              <div class=\"form-group label-floating \">
                  <label class=\"control-label\">Wallpaper resolution</label>
                  ";
        // line 94
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "resolution", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                  <span class=\"validate-input\">";
        // line 95
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "resolution", array()), 'errors');
        echo "</span>
              </div>
              <div class=\"form-group label-floating \">
                  <label class=\"control-label\">Wallpaper size</label>
                  ";
        // line 99
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "size", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                  <span class=\"validate-input\">";
        // line 100
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "size", array()), 'errors');
        echo "</span>
              </div>
              <span class=\"pull-right\"><a href=\"";
        // line 102
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_wallpaper_index");
        echo "\" class=\"btn btn-fill btn-yellow\"><i class=\"material-icons\">arrow_back</i> Cancel</a>";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "save", array()), 'widget', array("attr" => array("class" => "btn btn-fill btn-rose")));
        echo "</span>
            ";
        // line 103
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
          </div>
        </div>
      </div>
    </div>
  </div>
";
        
        $__internal_190ae36a385d3d7d37e275d138cf59925ce2449b89b6564f78cec79b1a8e07e5->leave($__internal_190ae36a385d3d7d37e275d138cf59925ce2449b89b6564f78cec79b1a8e07e5_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:Wallpaper:video_add_url.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  280 => 103,  274 => 102,  269 => 100,  265 => 99,  258 => 95,  254 => 94,  240 => 83,  236 => 82,  230 => 78,  219 => 75,  216 => 74,  212 => 73,  207 => 71,  203 => 69,  181 => 66,  167 => 65,  163 => 64,  158 => 62,  154 => 60,  143 => 57,  140 => 56,  136 => 55,  131 => 53,  124 => 49,  116 => 44,  108 => 39,  100 => 34,  96 => 33,  88 => 28,  83 => 26,  79 => 25,  72 => 21,  68 => 20,  61 => 16,  57 => 15,  51 => 12,  40 => 3,  34 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"AppBundle::layout.html.twig\" %}
{% block body%}
  <div class=\"container-fluid\">
    <div class=\"row\">
      <div class=\"col-sm-offset-1 col-md-10\">
        <div class=\"card\">
          <div class=\"card-header card-header-icon\" data-background-color=\"rose\">
            <i class=\"material-icons\">video_library</i>
          </div>
          <div class=\"card-content\">
            <h4 class=\"card-title\">New Wallpaper</h4>
            {{form_start(form)}}
               <div class=\"form-group label-floating \">
                  <label class=\"control-label\">Wallpaper title</label>
                  {{form_widget(form.title,{\"attr\":{\"class\":\"form-control\"}})}}
                  <span class=\"validate-input\">{{form_errors(form.title)}}</span>
              </div>
              <div class=\"form-group label-floating \">
                  <label class=\"control-label\">Wallpaper description</label>
                  {{form_widget(form.description,{\"attr\":{\"class\":\"form-control\"}})}}
                  <span class=\"validate-input\">{{form_errors(form.description)}}</span>
              </div>
              <div class=\"form-group label-floating \">
                  <label class=\"control-label\">Wallpaper url</label>
                  {{form_widget(form.urlvideo,{\"attr\":{\"class\":\"form-control\"}})}}
                  <span class=\"validate-input\">{{form_errors(form.urlvideo)}}</span>
              </div>
               <img src=\"{{asset(\"img/image_placeholder.jpg\")}}\" class=\"fileinput-preview thumbnail \" id=\"img-preview\">
                <div class=\"fileinput fileinput-new text-center\" data-provides=\"fileinput\" style=\"width: 100%;\">
                    <div>
                        <a href=\"#\" class=\"btn btn-rose btn-round btn-select\" style=\"width: 100%;margin:0px;\"><i class=\"material-icons\">image</i> Select Image</a>
                    </div>
                    {{form_widget(form.file,{\"attr\":{\"class\":\"file-hidden input-file img-selector\",\"style\":\"   /* display: none; */height: 0px;width: 0px;position: absolute;\"}})}}
                    <span class=\"validate-input\">{{form_errors(form.file)}}</span>
               </div>
               
              <div class=\"\">
                    <label>
                      {{form_widget(form.premium)}}  Premium wallpaper
                    </label>
              </div>
              <div class=\"\">
                    <label>
                      {{form_widget(form.enabled)}}  Enabled
                    </label>
              </div>
              <div class=\"\">
                    <label>
                      {{form_widget(form.comment)}}  Enabled Comment
                    </label>
              </div>
              <br>
              {{form_label(form.categories,null,{label_attr:{\"style\":\"font-size:16px\"}})}} :
              <div>
                 {% for field in form.categories %}
                      <label   class=\"color-label\" >
                          <span class=\"color-label-checkbox\" >{{ form_widget(field) }}</span ><span class=\"color-label-text\">{{ field.vars.label }}</span>
                      </label>
                  {% endfor %}
              </div>
              <br>
              {{form_label(form.colors,null,{label_attr:{\"style\":\"font-size:16px\"}})}} :
              <div>
                 {% for field in form.colors %}
                      <label   class=\"color-label\" style=\"background: #{% for color in colors %}{% if color.id == field.vars.value %}{{color.code}}{% endif %}{% endfor %}\">
                          <span class=\"color-label-checkbox\" style=\"background: #{% for color in colors %}{% if color.id == field.vars.value %}{{color.code}}{% endif %}{% endfor %}\">{{ form_widget(field) }}</span ><span class=\"color-label-text\">{{ field.vars.label }}</span>
                      </label>
                  {% endfor %}
              </div>
              <br>
              {{form_label(form.packs,null,{label_attr:{\"style\":\"font-size:16px\"}})}} :
              <div>
                 {% for field in form.packs %}
                      <label   class=\"color-label\" >
                          <span class=\"color-label-checkbox\" >{{ form_widget(field) }}</span ><span class=\"color-label-text\">{{ field.vars.label }}</span>
                      </label>
                  {% endfor %}
              </div><br>
              <div class=\"form-group label-floating \">
                  <label class=\"control-label\">Wallpaper tags (Ex:anim,art,hero)</label>
                  <br>
                  {{form_widget(form.tags,{\"attr\":{\"class\":\"input-tags\"}})}}
                  <span class=\"validate-input\">{{form_errors(form.tags)}}</span>
              </div>
              <script>
              \$('.input-tags').selectize({
                persist: false,
                createOnBlur: true,
                create: true
              });
              </script>
              <div class=\"form-group label-floating \">
                  <label class=\"control-label\">Wallpaper resolution</label>
                  {{form_widget(form.resolution,{\"attr\":{\"class\":\"form-control\"}})}}
                  <span class=\"validate-input\">{{form_errors(form.resolution)}}</span>
              </div>
              <div class=\"form-group label-floating \">
                  <label class=\"control-label\">Wallpaper size</label>
                  {{form_widget(form.size,{\"attr\":{\"class\":\"form-control\"}})}}
                  <span class=\"validate-input\">{{form_errors(form.size)}}</span>
              </div>
              <span class=\"pull-right\"><a href=\"{{path(\"app_wallpaper_index\")}}\" class=\"btn btn-fill btn-yellow\"><i class=\"material-icons\">arrow_back</i> Cancel</a>{{form_widget(form.save,{attr:{\"class\":\"btn btn-fill btn-rose\"}})}}</span>
            {{form_end(form)}}
          </div>
        </div>
      </div>
    </div>
  </div>
{% endblock%}", "AppBundle:Wallpaper:video_add_url.html.twig", "/home/kaman/projects/Web/src/AppBundle/Resources/views/Wallpaper/video_add_url.html.twig");
    }
}
