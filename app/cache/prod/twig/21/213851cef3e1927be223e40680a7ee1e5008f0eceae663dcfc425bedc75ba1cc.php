<?php

/* AppBundle:Home:index.html.twig */
class __TwigTemplate_09041043c1d45117bb439c9b4ff3dfac14438703a59b2c5c1a5cfd71d1a79ef3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AppBundle::layout.html.twig", "AppBundle:Home:index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AppBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 3
        echo "<div class=\"container-fluid\">
    <div class=\"row\">
        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"red\">
                    <i class=\"material-icons\">devices_other</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">Installs</p>
                     <h3 class=\"title\">";
        // line 12
        echo twig_escape_filter($this->env, ($context["devices_count"] ?? null), "html", null, true);
        echo "</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">perm_device_information</i><span> Application install</span> 
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"green\">
                    <i class=\"material-icons\">format_paint</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">Sets</p>
                     <h3 class=\"title\">";
        // line 28
        echo twig_escape_filter($this->env, ($context["count_sets"] ?? null), "html", null, true);
        echo "</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">keyboard_arrow_right</i><a href=\"";
        // line 32
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_wallpaper_index");
        echo "\">Wallpaper list</a>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"green\">
                    <i class=\"material-icons\">cloud_download</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">Downloads</p>
                     <h3 class=\"title\">";
        // line 44
        echo twig_escape_filter($this->env, ($context["count_downloads"] ?? null), "html", null, true);
        echo "</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">keyboard_arrow_right</i><a href=\"";
        // line 48
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_wallpaper_index");
        echo "\">Wallpaper list</a>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"green\">
                    <i class=\"material-icons\">share</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">Shares</p>
                     <h3 class=\"title\">";
        // line 60
        echo twig_escape_filter($this->env, ($context["count_shares"] ?? null), "html", null, true);
        echo "</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">keyboard_arrow_right</i><a href=\"";
        // line 64
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_wallpaper_index");
        echo "\">Wallpaper list</a>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"green\">
                    <i class=\"material-icons\">remove_red_eye</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">Views</p>
                     <h3 class=\"title\">";
        // line 76
        echo twig_escape_filter($this->env, ($context["count_views"] ?? null), "html", null, true);
        echo "</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">keyboard_arrow_right</i><a href=\"";
        // line 80
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_wallpaper_index");
        echo "\">Wallpaper list</a>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"rose\">
                    <i class=\"material-icons\">comment</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">Comments</p>
                     <h3 class=\"title\">";
        // line 92
        echo twig_escape_filter($this->env, ($context["comment_count"] ?? null), "html", null, true);
        echo "</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">keyboard_arrow_right</i><a href=\"";
        // line 96
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_comment_index");
        echo "\">Comments list</a>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"green\">
                    <i class=\"material-icons\">video_library</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">Videos</p>
                     <h3 class=\"title\">";
        // line 108
        echo twig_escape_filter($this->env, ($context["video_count"] ?? null), "html", null, true);
        echo "</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">keyboard_arrow_right</i><a href=\"";
        // line 112
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_wallpaper_index");
        echo "\">Wallpaper list</a>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"green\">
                    <i class=\"material-icons\">gif</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">Gifs</p>
                     <h3 class=\"title\">";
        // line 124
        echo twig_escape_filter($this->env, ($context["gif_count"] ?? null), "html", null, true);
        echo "</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">keyboard_arrow_right</i><a href=\"";
        // line 128
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_wallpaper_index");
        echo "\">Wallpaper list</a>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"green\">
                    <i class=\"material-icons\">image</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">Images</p>
                     <h3 class=\"title\">";
        // line 140
        echo twig_escape_filter($this->env, ($context["image_count"] ?? null), "html", null, true);
        echo "</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">keyboard_arrow_right</i><a href=\"";
        // line 144
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_wallpaper_index");
        echo "\">Wallpaper list</a>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"blue\">
                    <i class=\"material-icons\">access_time</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">Reviews</p>
                     <h3 class=\"title\">";
        // line 156
        echo twig_escape_filter($this->env, ($context["review_count"] ?? null), "html", null, true);
        echo "</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">keyboard_arrow_right</i><a href=\"";
        // line 160
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_wallpaper_reviews");
        echo "\">Wallpaper to reviews</a>
                    </div>
                </div>
            </div>
        </div>


        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"red\">
                    <i class=\"material-icons\">view_list</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">Categoryies </p>
                     <h3 class=\"title\">";
        // line 174
        echo twig_escape_filter($this->env, ($context["category_count"] ?? null), "html", null, true);
        echo "</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">keyboard_arrow_right</i><a href=\"";
        // line 178
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_category_index");
        echo "\">Categories list</a>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"red\">
                    <i class=\"material-icons\">inbox</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">Packs</p>
                     <h3 class=\"title\">";
        // line 190
        echo twig_escape_filter($this->env, ($context["pack_count"] ?? null), "html", null, true);
        echo "</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">keyboard_arrow_right</i><a href=\"";
        // line 194
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_pack_index");
        echo "\">packs list</a>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"red\">
                    <i class=\"material-icons\">palette</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">Colors</p>
                     <h3 class=\"title\">";
        // line 206
        echo twig_escape_filter($this->env, ($context["color_count"] ?? null), "html", null, true);
        echo "</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">keyboard_arrow_right</i><a href=\"";
        // line 210
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_color_index");
        echo "\">Colors list</a>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"purple\">
                    <i class=\"material-icons\">group</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">users</p>
                     <h3 class=\"title\">";
        // line 222
        echo twig_escape_filter($this->env, ($context["users_count"] ?? null), "html", null, true);
        echo "</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">keyboard_arrow_right</i><a href=\"";
        // line 226
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("user_user_index");
        echo "\">user list</a>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"rose\">
                    <i class=\"material-icons\">help</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">Supports</p>
                     <h3 class=\"title\">";
        // line 238
        echo twig_escape_filter($this->env, ($context["supports_count"] ?? null), "html", null, true);
        echo "</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">keyboard_arrow_right</i><a href=\"";
        // line 242
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_support_index");
        echo "\">Support messages list</a>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-4 col-md-6 col-sm-6\">
            <div class=\"card card-stats\">
                <div class=\"card-header\" data-background-color=\"black\">
                    <i class=\"material-icons\">info</i>
                </div>
                <div class=\"card-content\">
                    <p class=\"category\">Version</p>
                     <h3 class=\"title\">";
        // line 254
        echo twig_escape_filter($this->env, ($context["version_count"] ?? null), "html", null, true);
        echo "</h3>
                </div>
                <div class=\"card-footer\">
                    <div class=\"stats\">
                        <i class=\"material-icons\">keyboard_arrow_right</i><a href=\"";
        // line 258
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_version_index");
        echo "\">Versions list</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "AppBundle:Home:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  378 => 258,  371 => 254,  356 => 242,  349 => 238,  334 => 226,  327 => 222,  312 => 210,  305 => 206,  290 => 194,  283 => 190,  268 => 178,  261 => 174,  244 => 160,  237 => 156,  222 => 144,  215 => 140,  200 => 128,  193 => 124,  178 => 112,  171 => 108,  156 => 96,  149 => 92,  134 => 80,  127 => 76,  112 => 64,  105 => 60,  90 => 48,  83 => 44,  68 => 32,  61 => 28,  42 => 12,  31 => 3,  28 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "AppBundle:Home:index.html.twig", "/home/kaman/projects/Web/src/AppBundle/Resources/views/Home/index.html.twig");
    }
}
