<?php

/* AppBundle:Wallpaper:video_add.html.twig */
class __TwigTemplate_5a46e8954d88009a22455d8e434df6ce9db047d3a3091ecec6da34b659813776 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AppBundle::layout.html.twig", "AppBundle:Wallpaper:video_add.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AppBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 3
        echo "  <div class=\"container-fluid\">
    <div class=\"row\">
      <div class=\"col-sm-offset-1 col-md-10\">
        <div class=\"card\">
          <div class=\"card-header card-header-icon\" data-background-color=\"rose\">
            <i class=\"material-icons\">video_library</i>
          </div>
          <div class=\"card-content\">
            <h4 class=\"card-title\">New Wallpaper Video</h4>
            ";
        // line 12
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["form"] ?? null), 'form_start');
        echo "
                <br>
               <div class=\"form-group label-floating \">
                  <label class=\"control-label\">Wallpaper title</label>
                  ";
        // line 16
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "title", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                  <span class=\"validate-input\">";
        // line 17
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "title", array()), 'errors');
        echo "</span>
              </div>
              <div class=\"form-group label-floating \">
                  <label class=\"control-label\">Wallpaper description</label>
                  ";
        // line 21
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "description", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                  <span class=\"validate-input\">";
        // line 22
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "description", array()), 'errors');
        echo "</span>
              </div>
               <img src=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/image_placeholder.jpg"), "html", null, true);
        echo "\" class=\"fileinput-preview thumbnail \" id=\"img-preview\">
                <div class=\"fileinput fileinput-new text-center\" data-provides=\"fileinput\" style=\"width: 100%;\">
                    <div>
                        <a href=\"#\" class=\"btn btn-rose btn-round btn-select\" style=\"width: 100%;margin:0px;\"><i class=\"material-icons\">image</i> Select Image</a>
                    </div>
                    ";
        // line 29
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "file", array()), 'widget', array("attr" => array("class" => "file-hidden input-file img-selector", "style" => "   /* display: none; */height: 0px;width: 0px;position: absolute;")));
        echo "
                    <span class=\"validate-input\">";
        // line 30
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "file", array()), 'errors');
        echo "</span>
               </div>
               <br>
               <video width=\"100%\" style=\"display:none\" controls>
                      <source id=\"video_here\">
                        Your browser does not support HTML5 video.
                     </video>
                      <div class=\"text-center\"  style=\"width: 100%;\">
                         
                          <div>
                              <a href=\"#\" class=\"btn btn-rose btn-round select-video\" style=\"width: 100%;\"><i class=\"material-icons\">videocam</i> Select Video</a>
                          </div>
                          ";
        // line 42
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "filevideo", array()), 'widget', array("attr" => array("class" => "input-video", "style" => "   /* display: none; */height: 0px;width: 0px;position: absolute;")));
        echo "
                          <span class=\"validate-input\">";
        // line 43
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "filevideo", array()), 'errors');
        echo "</span>
                     </div>
                      <br>
               <br>
              <div class=\"\">
                    <label>
                      ";
        // line 49
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "premium", array()), 'widget');
        echo "  Premium wallpaper
                    </label>
              </div>
              <div class=\"\">
                    <label>
                      ";
        // line 54
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "enabled", array()), 'widget');
        echo "  Enabled
                    </label>
              </div>
              <div class=\"\">
                    <label>
                      ";
        // line 59
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "comment", array()), 'widget');
        echo "  Enabled Comment
                    </label>
              </div>
              <br>
              ";
        // line 63
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "categories", array()), 'label', array("label_attr" => array("style" => "font-size:16px")));
        echo " :
              <div>
                 ";
        // line 65
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["form"] ?? null), "categories", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["field"]) {
            // line 66
            echo "                      <label   class=\"color-label\" >
                          <span class=\"color-label-checkbox\" >";
            // line 67
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($context["field"], 'widget');
            echo "</span ><span class=\"color-label-text\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["field"], "vars", array()), "label", array()), "html", null, true);
            echo "</span>
                      </label>
                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 70
        echo "              </div>
              <br>
              ";
        // line 72
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "colors", array()), 'label', array("label_attr" => array("style" => "font-size:16px")));
        echo " :
              <div>
                 ";
        // line 74
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["form"] ?? null), "colors", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["field"]) {
            // line 75
            echo "                      <label   class=\"color-label\" style=\"background: #";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["colors"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["color"]) {
                if (($this->getAttribute($context["color"], "id", array()) == $this->getAttribute($this->getAttribute($context["field"], "vars", array()), "value", array()))) {
                    echo twig_escape_filter($this->env, $this->getAttribute($context["color"], "code", array()), "html", null, true);
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['color'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo "\">
                          <span class=\"color-label-checkbox\" style=\"background: #";
            // line 76
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["colors"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["color"]) {
                if (($this->getAttribute($context["color"], "id", array()) == $this->getAttribute($this->getAttribute($context["field"], "vars", array()), "value", array()))) {
                    echo twig_escape_filter($this->env, $this->getAttribute($context["color"], "code", array()), "html", null, true);
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['color'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo "\">";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($context["field"], 'widget');
            echo "</span ><span class=\"color-label-text\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["field"], "vars", array()), "label", array()), "html", null, true);
            echo "</span>
                      </label>
                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 79
        echo "              </div>
              <br>
              ";
        // line 81
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "packs", array()), 'label', array("label_attr" => array("style" => "font-size:16px")));
        echo " :
              <div>
                 ";
        // line 83
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["form"] ?? null), "packs", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["field"]) {
            // line 84
            echo "                      <label   class=\"color-label\" >
                          <span class=\"color-label-checkbox\" >";
            // line 85
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($context["field"], 'widget');
            echo "</span ><span class=\"color-label-text\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["field"], "vars", array()), "label", array()), "html", null, true);
            echo "</span>
                      </label>
                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 88
        echo "              </div><br>
                <div class=\"form-group label-floating \">
                    <label class=\"control-label\">Wallpaper tags (Ex:anim,art,hero)</label>
                    <br>
                    ";
        // line 92
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "tags", array()), 'widget', array("attr" => array("class" => "input-tags")));
        echo "
                    <span class=\"validate-input\">";
        // line 93
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "tags", array()), 'errors');
        echo "</span>
                </div>
                <script>
                \$('.input-tags').selectize({
                  persist: false,
                  createOnBlur: true,
                  create: true
                });
                </script>
              <div class=\"form-group label-floating \">
                  <label class=\"control-label\">Wallpaper resolution</label>
                  ";
        // line 104
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "resolution", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                  <span class=\"validate-input\">";
        // line 105
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "resolution", array()), 'errors');
        echo "</span>
              </div>
              <span class=\"pull-right\"><a href=\"";
        // line 107
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_wallpaper_index");
        echo "\" class=\"btn btn-fill btn-yellow\"><i class=\"material-icons\">arrow_back</i> Cancel</a>";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "save", array()), 'widget', array("attr" => array("class" => "btn btn-fill btn-rose")));
        echo "</span>
            ";
        // line 108
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["form"] ?? null), 'form_end');
        echo "
          </div>
        </div>
      </div>
    </div>
  </div>
";
    }

    public function getTemplateName()
    {
        return "AppBundle:Wallpaper:video_add.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  270 => 108,  264 => 107,  259 => 105,  255 => 104,  241 => 93,  237 => 92,  231 => 88,  220 => 85,  217 => 84,  213 => 83,  208 => 81,  204 => 79,  182 => 76,  168 => 75,  164 => 74,  159 => 72,  155 => 70,  144 => 67,  141 => 66,  137 => 65,  132 => 63,  125 => 59,  117 => 54,  109 => 49,  100 => 43,  96 => 42,  81 => 30,  77 => 29,  69 => 24,  64 => 22,  60 => 21,  53 => 17,  49 => 16,  42 => 12,  31 => 3,  28 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "AppBundle:Wallpaper:video_add.html.twig", "/home/kaman/projects/Web/src/AppBundle/Resources/views/Wallpaper/video_add.html.twig");
    }
}
