<?php

/* AppBundle:Home:settings.html.twig */
class __TwigTemplate_b9c1900216ce7d8a52a0e3569912ecd44c4052d279199c9169f44f4302d85061 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AppBundle::layout.html.twig", "AppBundle:Home:settings.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AppBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 3
        echo "<div class=\"container-fluid\">
    <div class=\"row\">
        ";
        // line 5
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["form"] ?? null), 'form_start');
        echo "
        <div class=\"col-md-12\">
            <div class=\"card\">
                <div class=\"card-header card-header-icon\" data-background-color=\"rose\">
                    <i class=\"material-icons\">settings</i>
                </div>
                <br>
                <h4 class=\"card-title\">Settings</h4>
            </div>
        </div>
       <div class=\"col-md-12\">
            <div class=\"card\">
                <div class=\"card-content\">              
                        <div class=\"form-group label-floating is-empty\" style=\"padding: 0px;margin: 0px;\">
                            <label class=\"control-label\">Firebase Legcy sever key</label>
                            ";
        // line 20
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "firebasekey", array()), 'widget', array("attr" => array("class" => "form-control", "rows" => 12)));
        echo "
                            <span class=\"validate-input\">";
        // line 21
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "firebasekey", array()), 'errors');
        echo "</span>
                        </div>
                </div>
                <div class=\"card\" style=\"    margin-top: -15px;\">
                    <span class=\"pull-right\"><a href=\"";
        // line 25
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_home_index");
        echo "\" class=\"btn btn-fill btn-yellow col-md-6\"><i class=\"material-icons\">arrow_back</i> Cancel</a>";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "save", array()), 'widget', array("attr" => array("class" => "btn btn-fill btn-rose")));
        echo "</span>
                </div>
            </div>
        </div>
        ";
        // line 29
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["form"] ?? null), 'form_end');
        echo "
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "AppBundle:Home:settings.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 29,  64 => 25,  57 => 21,  53 => 20,  35 => 5,  31 => 3,  28 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "AppBundle:Home:settings.html.twig", "/home/kaman/projects/Web/src/AppBundle/Resources/views/Home/settings.html.twig");
    }
}
