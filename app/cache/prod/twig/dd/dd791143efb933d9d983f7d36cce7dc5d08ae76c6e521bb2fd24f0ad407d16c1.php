<?php

/* AppBundle:Wallpaper:reviews.html.twig */
class __TwigTemplate_3d24ad0f7f3ca96efa9a875e22cc2713266e397cacba367f8a352623c3a76c8c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AppBundle::layout.html.twig", "AppBundle:Wallpaper:reviews.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AppBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 3
        echo "<div class=\"container-fluid\">
\t\t<div class=\"row\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-4\" style=\"padding-right:10px;padding-left:10px;\">
\t\t\t\t\t<a href=\"";
        // line 7
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_wallpaper_reviews");
        echo "\" class=\"btn  btn-lg btn-warning col-md-12\"><i class=\"material-icons\" style=\"font-size: 30px;\">refresh</i> Refresh</a>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-4\" style=\"padding-right:10px;padding-left:10px;\">
\t\t\t\t\t<a class=\"btn btn btn-lg btn-yellow col-md-12\"><i class=\"material-icons\" style=\"font-size: 30px;\">wallpaper</i> ";
        // line 10
        echo twig_escape_filter($this->env, ($context["wallpapers_count"] ?? null), "html", null, true);
        echo " WALL TO REVIEW</a>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-4\" style=\"padding-right:10px;padding-left:10px;\">
\t\t\t\t\t<div class=\"dropdown\">
\t\t\t\t\t\t<a href=\"#\" data-toggle=\"dropdown\" aria-expanded=\"false\" class=\"btn btn-rose btn-lg pull-right add-button col-md-12\"title=\"\"><i class=\"material-icons\" style=\"font-size: 30px;\">add_box</i> NEW WALLPAPER </a>
\t\t\t\t\t\t<ul class=\"dropdown-menu dropdown-menu-right\" role=\"menu\">
\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<a href=\"";
        // line 17
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_image_add");
        echo "\"><i class=\"material-icons\">image</i> UPLOAD WALLPAPER IMAGE</a>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<a href=\"";
        // line 20
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_gif_add");
        echo "\"><i class=\"material-icons\">gif</i> UPLOAD WALLPAPER GIF</a>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<a href=\"";
        // line 23
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_video_add");
        echo "\"><i class=\"material-icons\">cloud_upload</i> UPLOAD WALLPAPER VIDEO</a>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<a href=\"";
        // line 26
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_video_addurl");
        echo "\"><i class=\"material-icons\">link</i> ADD URL WALLPAPER VIDEO </a>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"row\">
       \t\t\t\t";
        // line 33
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["wallpapers"] ?? null));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["wallpaper"]) {
            // line 34
            echo "\t\t\t\t\t<div class=\"col-md-4 col-lg-3\" style=\"height:400px;padding-right:10px;padding-left:10px;\">
\t\t\t\t\t\t<div class=\"card card-product\"  >
\t\t\t\t\t\t\t<img  class=\"wallpaper-image\" src=\"";
            // line 36
            echo twig_escape_filter($this->env, $this->env->getExtension('Liip\ImagineBundle\Templating\ImagineExtension')->filter($this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($this->getAttribute($this->getAttribute($context["wallpaper"], "media", array()), "link", array())), "wallpaper_thumb"), "html", null, true);
            echo "\">
\t\t\t\t\t\t\t<div class=\"wallpaper-title\" >
\t\t\t\t\t\t\t\t";
            // line 38
            echo twig_escape_filter($this->env, $this->getAttribute($context["wallpaper"], "title", array()), "html", null, true);
            echo "
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"card-content\" style=\" padding: 0px 0px;\">
\t\t\t\t\t\t\t\t<div class=\"card-actions\">
\t\t\t\t\t\t\t\t\t<a href=\"";
            // line 42
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_wallpaper_review", array("id" => $this->getAttribute($context["wallpaper"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-info btn-simple btn-sm\" rel=\"tooltip\" data-placement=\"bottom\" title=\"\" data-original-title=\"Review\">
\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\">check</i>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t<a href=\"";
            // line 45
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_wallpaper_view", array("id" => $this->getAttribute($context["wallpaper"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-info btn-simple btn-sm\" rel=\"tooltip\" data-placement=\"bottom\" title=\"\" data-original-title=\"View\">
\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\">remove_red_eye</i>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t<a href=\"";
            // line 48
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_wallpaper_delete", array("id" => $this->getAttribute($context["wallpaper"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-danger btn-simple btn-sm\" rel=\"tooltip\" data-placement=\"bottom\" title=\"\" data-original-title=\"Delete\">
\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\">close</i>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t";
            // line 51
            if (($this->getAttribute($context["wallpaper"], "type", array()) == "image")) {
                // line 52
                echo "\t\t\t\t\t\t\t\t\t\t<a class=\"pull-right\" style=\"background-color: #00000000 !important;color:white;\"><i class=\"material-icons\">image</i></a>
\t\t\t\t\t\t\t\t\t";
            } elseif (($this->getAttribute(            // line 53
$context["wallpaper"], "type", array()) == "gif")) {
                // line 54
                echo "\t\t\t\t\t\t\t\t\t\t<a class=\"pull-right\" style=\"background-color: #00000000 !important;color:white;\"><i class=\"material-icons\">gif</i></a>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 55
                echo "\t    
\t\t\t\t\t\t\t\t\t\t<a class=\"pull-right\" style=\"background-color: #00000000 !important;color:white;\"><i class=\"material-icons\">videocam</i></a>
\t\t\t\t\t\t\t\t\t";
            }
            // line 58
            echo "\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"card-footer\">
\t\t\t\t\t\t\t\t<div class=\"price\">
\t\t\t\t\t\t\t\t\t<div class=\"wallpaper-logo\" >
\t\t\t\t\t\t\t\t\t\t";
            // line 63
            if (($this->getAttribute($this->getAttribute($context["wallpaper"], "user", array()), "image", array()) == "")) {
                // line 64
                echo "\t\t\t\t\t\t\t\t\t\t\t";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["wallpaper"], "user", array()), "name", array()), "html", null, true);
                echo "
\t\t\t\t\t\t\t\t\t\t";
            } else {
                // line 66
                echo "\t\t\t\t\t\t\t\t\t\t\t<img src=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["wallpaper"], "user", array()), "image", array()), "html", null, true);
                echo "\" class=\"avatar-img\" alt=\"\">
\t\t\t\t\t\t\t\t\t\t";
            }
            // line 68
            echo "\t\t\t\t\t\t\t\t\t\t<span>";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["wallpaper"], "user", array()), "name", array()), "html", null, true);
            echo "</span>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"stats pull-right\">
\t\t\t\t\t\t\t\t\t<div class=\"wallpaper-logo\" >";
            // line 72
            echo $this->env->getExtension('Knp\Bundle\TimeBundle\Twig\Extension\TimeExtension')->diff($this->getAttribute($context["wallpaper"], "created", array()));
            echo "</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 78
            echo "\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t<div class=\"card\">
\t\t\t\t\t\t\t<div class=\"card-content\">
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<center><img src=\"";
            // line 83
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/bg_empty.png"), "html", null, true);
            echo "\"  style=\"width: auto !important;\" =\"\"></center>
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['wallpaper'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 90
        echo "       \t\t\t</div>
            <div class=\" pull-right\">
        ";
        // line 92
        echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->render($this->env, ($context["wallpapers"] ?? null));
        echo "
      </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "AppBundle:Wallpaper:reviews.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  200 => 92,  196 => 90,  183 => 83,  176 => 78,  165 => 72,  157 => 68,  151 => 66,  145 => 64,  143 => 63,  136 => 58,  131 => 55,  127 => 54,  125 => 53,  122 => 52,  120 => 51,  114 => 48,  108 => 45,  102 => 42,  95 => 38,  90 => 36,  86 => 34,  81 => 33,  71 => 26,  65 => 23,  59 => 20,  53 => 17,  43 => 10,  37 => 7,  31 => 3,  28 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "AppBundle:Wallpaper:reviews.html.twig", "/home/kaman/projects/Web/src/AppBundle/Resources/views/Wallpaper/reviews.html.twig");
    }
}
