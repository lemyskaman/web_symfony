<?php

/* AppBundle:Wallpaper:view.html.twig */
class __TwigTemplate_351d6963a54d8184532972bbc9a0cf09cfca7286616915ff6d6c6d6258401980 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AppBundle::layout.html.twig", "AppBundle:Wallpaper:view.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AppBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 3
        echo "<div class=\"container-fluid\">
    <div class=\"row\">
       <div class=\"col-sm-offset-1 col-md-10\">
            <div class=\"card\">
                <div class=\"card-header card-header-icon\" data-background-color=\"rose\">
                    ";
        // line 8
        if (($this->getAttribute(($context["wallpaper"] ?? null), "type", array()) == "image")) {
            // line 9
            echo "                        <i class=\"material-icons\">image</i>
                    ";
        } elseif (($this->getAttribute(        // line 10
($context["wallpaper"] ?? null), "type", array()) == "gif")) {
            // line 11
            echo "                        <i class=\"material-icons\">gif</i>
                    ";
        } elseif (($this->getAttribute(        // line 12
($context["wallpaper"] ?? null), "type", array()) == "video")) {
            // line 13
            echo "                        <i class=\"material-icons\">videocam</i>
                    ";
        }
        // line 15
        echo "                </div>
                <div class=\"card-content\">
                    <h4 class=\"card-title\">";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute(($context["wallpaper"] ?? null), "title", array()), "html", null, true);
        echo "</h4>
                    <br>
                    <p >";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute(($context["wallpaper"] ?? null), "description", array()), "html", null, true);
        echo "</p>
                    ";
        // line 20
        if (($this->getAttribute(($context["wallpaper"] ?? null), "type", array()) == "video")) {
            // line 21
            echo "                        <video width=\"100%\" src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($this->getAttribute($this->getAttribute(($context["wallpaper"] ?? null), "video", array()), "link", array())), "html", null, true);
            echo "\" controls>
                            <source id=\"video_here\">
                            Your browser does not support HTML5 wallpaper.
                        </video>
                    ";
        }
        // line 26
        echo "                    ";
        if (($this->getAttribute(($context["wallpaper"] ?? null), "media", array()) != null)) {
            // line 27
            echo "                    <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($this->getAttribute($this->getAttribute(($context["wallpaper"] ?? null), "media", array()), "link", array())), "html", null, true);
            echo "\" class=\"fileinput-preview thumbnail \" id=\"img-preview\">
                    ";
        }
        // line 29
        echo "
                    <div class=\"row\">
                        <div class=\"col-md-6\">";
        // line 31
        if ($this->getAttribute(($context["wallpaper"] ?? null), "enabled", array())) {
            // line 32
            echo "                        <i class=\"material-icons\" style=\"color:green;float:left\">check_circle</i> <span class=\"check-label\">Enabled</span>
                        ";
        } else {
            // line 34
            echo "                        <i class=\"material-icons\" style=\"color:red;float:left\">cancel</i> <span class=\"check-label\">Disabled</span>
                        ";
        }
        // line 35
        echo "</div>
                    </div>
                     <div class=\"row\">
                        <div class=\"col-md-6\">";
        // line 38
        if ($this->getAttribute(($context["wallpaper"] ?? null), "premium", array())) {
            // line 39
            echo "                        <i class=\"material-icons\" style=\"color:green;float:left\">check_circle</i> <span class=\"check-label\">Premuim</span>
                        ";
        } else {
            // line 41
            echo "                        <i class=\"material-icons\" style=\"color:yellow;float:left\">cancel</i> <span class=\"check-label\">Free</span>
                        ";
        }
        // line 42
        echo "</div>
                    </div>
                     <div class=\"row\">
                        <div class=\"col-md-6\">";
        // line 45
        if ($this->getAttribute(($context["wallpaper"] ?? null), "comment", array())) {
            // line 46
            echo "                        <i class=\"material-icons\" style=\"color:green;float:left\">check_circle</i> <span class=\"check-label\">Comment Enabled</span>
                        ";
        } else {
            // line 48
            echo "                        <i class=\"material-icons\" style=\"color:red;float:left\">cancel</i> <span class=\"check-label\">Comment Disabled</span>
                        ";
        }
        // line 49
        echo "</div>
                    </div>

                    <div class=\"row\">
                        <div class=\"col-md-12\">
                            <h4>Categories : </h4>
                        </div>
                        <div class=\"col-md-12\" >
                            ";
        // line 57
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["wallpaper"] ?? null), "categories", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 58
            echo "                                <label   class=\"color-label\" >
                                      <span class=\"color-label-checkbox\" >x</span ><span class=\"color-label-text\"> ";
            // line 59
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "title", array()), "html", null, true);
            echo "</span>
                                </label>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 62
        echo "                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-12\">
                            <h4>Languages : </h4>
                        </div>
                        <div class=\"col-md-12\" >
                            ";
        // line 69
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["wallpaper"] ?? null), "colors", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["color"]) {
            // line 70
            echo "                              <label   class=\"color-label\" style=\"background: #";
            echo twig_escape_filter($this->env, $this->getAttribute($context["color"], "code", array()), "html", null, true);
            echo "\">
                                  <span class=\"color-label-checkbox\" style=\"background: #";
            // line 71
            echo twig_escape_filter($this->env, $this->getAttribute($context["color"], "code", array()), "html", null, true);
            echo "\">.</span ><span class=\"color-label-text\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["color"], "title", array()), "html", null, true);
            echo "</span>
                              </label>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['color'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 74
        echo "                        </div>
                    </div>
                    <br>


                 </div>
                <div class=\"card-footer\">
                    <div class=\"price\">
                         <div class=\"wallpaper-logo\" style=\"color:#040303\" >
                        ";
        // line 83
        if (($this->getAttribute($this->getAttribute(($context["wallpaper"] ?? null), "user", array()), "image", array()) == "")) {
            // line 84
            echo "                            ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["wallpaper"] ?? null), "user", array()), "name", array()), "html", null, true);
            echo "
                        ";
        } else {
            // line 86
            echo "                            <img src=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["wallpaper"] ?? null), "user", array()), "image", array()), "html", null, true);
            echo "\" class=\"avatar-img\" alt=\"\"> 
                        ";
        }
        // line 88
        echo "                         <span>";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["wallpaper"] ?? null), "user", array()), "name", array()), "html", null, true);
        echo "</span>
                     </div>
                    </div>
                    <div class=\"stats pull-right\">
                       <div class=\"wallpaper-logo\"  style=\"color:#040303\" >";
        // line 92
        echo $this->env->getExtension('Knp\Bundle\TimeBundle\Twig\Extension\TimeExtension')->diff($this->getAttribute(($context["wallpaper"] ?? null), "created", array()));
        echo "</div>
                    </div>
                </div>
            </div>
            <div class=\"row\">
            <div class=\"col-md-5\">
                    <div class=\"card\" >
                    <div class=\"card-content\">
                        <span class=\"label label-rose col-md-12\" style=\"font-size:13pt;padding:10px;background:#FF5722\"> <b>";
        // line 100
        echo twig_escape_filter($this->env, $this->getAttribute(($context["wallpaper"] ?? null), "viewsnumber", array()), "html", null, true);
        echo " </b></span>  <br><br>
                        <span class=\"label label-rose col-md-12\" style=\"font-size:13pt;padding:10px;background:#FF5722\"> <b>";
        // line 101
        echo twig_escape_filter($this->env, $this->getAttribute(($context["wallpaper"] ?? null), "downloadsnumber", array()), "html", null, true);
        echo " </b></span>  <br><br>
                        <span class=\"label label-rose col-md-12\" style=\"font-size:13pt;padding:10px;background:#FF5722\"> <b>";
        // line 102
        echo twig_escape_filter($this->env, $this->getAttribute(($context["wallpaper"] ?? null), "sharesnumber", array()), "html", null, true);
        echo " </b></span>  <br><br>
                        <span class=\"label label-rose col-md-12\" style=\"font-size:13pt;padding:10px;background:#FF5722\"> <b>";
        // line 103
        echo twig_escape_filter($this->env, $this->getAttribute(($context["wallpaper"] ?? null), "setsnumber", array()), "html", null, true);
        echo " </b></span>  <br><br>
                    </div>
                    </div>
            </div>
            <div class=\"col-md-7\">
                <div class=\"card\" >
                    <div class=\"status-bar\"></div>
                    <div class=\"action-bar\">
                        <a href=\"#\" class=\"zmdi zmdi-star\"></a>
                    </div>
                    ";
        // line 113
        $context["rate"] = ($context["rating"] ?? null);
        // line 114
        echo "                    ";
        $context["rate_main"] = ($context["rating"] ?? null);
        // line 115
        echo "                    <div class=\"list-group lg-alt lg-even-black\">
                        <br>
                        <center>
                        <span style=\"height: 28px;display: inline-block;font-size: 30pt;font-weight: bold;padding-left: 20px;\">Rating : ";
        // line 118
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, ($context["rate_main"] ?? null), 1, ".", ","), "html", null, true);
        echo "</span>
                        </center>
                        <table width=\"100%\" >
                            <tr>
                                <td colspan=\"3\" style=\"padding: 15px;\">
                                    <div style=\"/* float: left; */display: inline-flex;\">
                                        ";
        // line 124
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 125
            echo "                                            ";
            if ((($context["rate"] ?? null) >= 1)) {
                // line 126
                echo "                                                <img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/star.png"), "html", null, true);
                echo "\" style=\"height:50px;width:50px\">
                                            ";
            }
            // line 128
            echo "                                            ";
            if (((($context["rate"] ?? null) >= 0.25) && (($context["rate"] ?? null) < 0.75))) {
                // line 129
                echo "                                                <img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/star_h.png"), "html", null, true);
                echo "\" style=\"height:50px;width:50px\">
                                            ";
            }
            // line 131
            echo "                                            ";
            if (((($context["rate"] ?? null) >= 0.75) && (($context["rate"] ?? null) < 1))) {
                // line 132
                echo "                                                <img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/star.png"), "html", null, true);
                echo "\" style=\"height:50px;width:50px\">
                                            ";
            }
            // line 134
            echo "                                            ";
            if ((($context["rate"] ?? null) < 0.25)) {
                // line 135
                echo "                                                <img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/star_e.png"), "html", null, true);
                echo "\" style=\"height:50px;width:50px\">
                                            ";
            }
            // line 137
            echo "                                            ";
            $context["rate"] = (($context["rate"] ?? null) - 1);
            // line 138
            echo "                                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 139
        echo "                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td width=\"50%\" align=\"right\" style=\"padding: 5px;\">
                                    <img src=\"";
        // line 144
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/star.png"), "html", null, true);
        echo "\" style=\"height:30px;width:30px\">
                                    <img src=\"";
        // line 145
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/star.png"), "html", null, true);
        echo "\" style=\"height:30px;width:30px\">
                                    <img src=\"";
        // line 146
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/star.png"), "html", null, true);
        echo "\" style=\"height:30px;width:30px\">
                                    <img src=\"";
        // line 147
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/star.png"), "html", null, true);
        echo "\" style=\"height:30px;width:30px\">
                                    <img src=\"";
        // line 148
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/star.png"), "html", null, true);
        echo "\" style=\"height:30px;width:30px\">
                                </td>
                                <td width=\"30px\" align=\"center\">";
        // line 150
        echo twig_escape_filter($this->env, $this->getAttribute(($context["ratings"] ?? null), "rate_5", array()), "html", null, true);
        echo "</td>
                                <td  align=\"left\" style=\"padding:10px\">
                                    <span style=\"display:block;height:15px;background-color:#000000;border-radius:10px;width:";
        // line 152
        echo twig_escape_filter($this->env, $this->getAttribute(($context["values"] ?? null), "rate_5", array()), "html", null, true);
        echo "%\"></span>
                                </td>
                            </tr>
                            <tr>
                                <td width=\"50%\" align=\"right\" style=\"padding: 5px;\">
                                    <img src=\"";
        // line 157
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/star_e.png"), "html", null, true);
        echo "\" style=\"height:30px;width:30px\">
                                    <img src=\"";
        // line 158
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/star.png"), "html", null, true);
        echo "\" style=\"height:30px;width:30px\">
                                    <img src=\"";
        // line 159
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/star.png"), "html", null, true);
        echo "\" style=\"height:30px;width:30px\">
                                    <img src=\"";
        // line 160
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/star.png"), "html", null, true);
        echo "\" style=\"height:30px;width:30px\">
                                    <img src=\"";
        // line 161
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/star.png"), "html", null, true);
        echo "\" style=\"height:30px;width:30px\">
                                </td>
                                <td width=\"30px\" align=\"center\">";
        // line 163
        echo twig_escape_filter($this->env, $this->getAttribute(($context["ratings"] ?? null), "rate_4", array()), "html", null, true);
        echo "</td>
                                <td  align=\"left\" style=\"padding:10px\">
                                    <span style=\"display:block;height:15px;background-color:#000000;border-radius:10px;width:";
        // line 165
        echo twig_escape_filter($this->env, $this->getAttribute(($context["values"] ?? null), "rate_4", array()), "html", null, true);
        echo "%\"></span>
                                </td>
                            </tr>
                            <tr>
                                <td width=\"50%\" align=\"right\" style=\"padding: 5px;\">
                                    <img src=\"";
        // line 170
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/star_e.png"), "html", null, true);
        echo "\" style=\"height:30px;width:30px\">
                                    <img src=\"";
        // line 171
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/star_e.png"), "html", null, true);
        echo "\" style=\"height:30px;width:30px\">
                                    <img src=\"";
        // line 172
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/star.png"), "html", null, true);
        echo "\" style=\"height:30px;width:30px\">
                                    <img src=\"";
        // line 173
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/star.png"), "html", null, true);
        echo "\" style=\"height:30px;width:30px\">
                                    <img src=\"";
        // line 174
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/star.png"), "html", null, true);
        echo "\" style=\"height:30px;width:30px\">
                                </td>
                                <td width=\"30px\" align=\"center\">";
        // line 176
        echo twig_escape_filter($this->env, $this->getAttribute(($context["ratings"] ?? null), "rate_3", array()), "html", null, true);
        echo "</td>
                                <td  align=\"left\" style=\"padding:10px\">
                                    <span style=\"display:block;height:15px;background-color:#000000;border-radius:10px;width:";
        // line 178
        echo twig_escape_filter($this->env, $this->getAttribute(($context["values"] ?? null), "rate_3", array()), "html", null, true);
        echo "%\"></span>
                                </td>
                            </tr>
                            <tr>
                                <td width=\"50%\" align=\"right\" style=\"padding: 5px;\">
                                    
                                    <img src=\"";
        // line 184
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/star_e.png"), "html", null, true);
        echo "\" style=\"height:30px;width:30px\">
                                    <img src=\"";
        // line 185
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/star_e.png"), "html", null, true);
        echo "\" style=\"height:30px;width:30px\">
                                    <img src=\"";
        // line 186
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/star_e.png"), "html", null, true);
        echo "\" style=\"height:30px;width:30px\">
                                    <img src=\"";
        // line 187
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/star.png"), "html", null, true);
        echo "\" style=\"height:30px;width:30px\">
                                    <img src=\"";
        // line 188
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/star.png"), "html", null, true);
        echo "\" style=\"height:30px;width:30px\">
                                </td>
                                <td width=\"30px\" align=\"center\">";
        // line 190
        echo twig_escape_filter($this->env, $this->getAttribute(($context["ratings"] ?? null), "rate_2", array()), "html", null, true);
        echo "</td>
                                <td  align=\"left\" style=\"padding:10px\">
                                    <span style=\"display:block;height:15px;background-color:#000000;border-radius:10px;width:";
        // line 192
        echo twig_escape_filter($this->env, $this->getAttribute(($context["values"] ?? null), "rate_2", array()), "html", null, true);
        echo "%\"></span>
                                </td>
                            </tr>
                            <tr>
                                <td width=\"50%\" align=\"right\" style=\"padding: 5px;\">
                                    <img src=\"";
        // line 197
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/star_e.png"), "html", null, true);
        echo "\" style=\"height:30px;width:30px\">
                                    <img src=\"";
        // line 198
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/star_e.png"), "html", null, true);
        echo "\" style=\"height:30px;width:30px\">
                                    <img src=\"";
        // line 199
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/star_e.png"), "html", null, true);
        echo "\" style=\"height:30px;width:30px\">
                                    <img src=\"";
        // line 200
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/star_e.png"), "html", null, true);
        echo "\" style=\"height:30px;width:30px\">
                                    <img src=\"";
        // line 201
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/star.png"), "html", null, true);
        echo "\" style=\"height:30px;width:30px\">
                                </td>
                                <td width=\"30px\" align=\"center\">";
        // line 203
        echo twig_escape_filter($this->env, $this->getAttribute(($context["ratings"] ?? null), "rate_1", array()), "html", null, true);
        echo "</td>
                                <td  align=\"left\" style=\"padding:10px\">
                                    <span style=\"display:block;height:15px;background-color:#000000;border-radius:10px;width:";
        // line 205
        echo twig_escape_filter($this->env, $this->getAttribute(($context["values"] ?? null), "rate_1", array()), "html", null, true);
        echo "%\"></span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            </div>
            <div class=\"row\">
            ";
        // line 214
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["wallpaper"] ?? null), "comments", array()));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["comment"]) {
            // line 215
            echo "                <div class=\"col-md-6\">
                    <ul class=\"timeline timeline-simple\">
                        <li class=\"timeline-inverted\">
                            <div class=\"timeline-badge danger\">
                                <img src=\"";
            // line 219
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["comment"], "user", array()), "image", array()), "html", null, true);
            echo "\" class=\"img-profile\">
                            </div>
                            <div class=\"timeline-panel\">
                                <div class=\"timeline-heading\">
                                    <a href=\"";
            // line 223
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("user_user_edit", array("id" => $this->getAttribute($this->getAttribute($context["comment"], "user", array()), "id", array()))), "html", null, true);
            echo "\" title=\"\">
                                        <span class=\"label label-danger\">";
            // line 224
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["comment"], "user", array()), "name", array()), "html", null, true);
            echo "</span>
                                    </a>
                                    <span class=\"pull-right\" >
                                        <a href=\"";
            // line 227
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_comment_delete", array("id" => $this->getAttribute($context["comment"], "id", array()), "wallpaper" => "true")), "html", null, true);
            echo "\"  rel=\"tooltip\" data-placement=\"bottom\" title=\"\" data-original-title=\"Delete\">
                                            <i class=\"material-icons\" style=\"color:red\">delete</i>
                                        </a>
                                        ";
            // line 230
            if ($this->getAttribute($context["comment"], "enabled", array())) {
                // line 231
                echo "                                            <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_comment_hide", array("id" => $this->getAttribute($context["comment"], "id", array()))), "html", null, true);
                echo "\"  rel=\"tooltip\" data-placement=\"bottom\" title=\"\" data-original-title=\"Hide\">
                                                 <i class=\"material-icons\">visibility_off</i>
                                            </a>
                                        ";
            } else {
                // line 234
                echo "                                      
                                            <a href=\"";
                // line 235
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_comment_hide", array("id" => $this->getAttribute($context["comment"], "id", array()))), "html", null, true);
                echo "\"  rel=\"tooltip\" data-placement=\"bottom\" title=\"\" data-original-title=\"Show\">
                                                 <i class=\"material-icons\">remove_red_eye</i>
                                            </a>
                                        ";
            }
            // line 239
            echo "                                    </span>
                                </div>
                                <div class=\"timeline-body\">
                                    <p>";
            // line 242
            echo twig_escape_filter($this->env, $this->getAttribute($context["comment"], "contentclear", array()), "html", null, true);
            echo "</p>
                                </div>
                                <small class=\"pull-right label label-rose\">
                                     <span>";
            // line 245
            echo $this->env->getExtension('Knp\Bundle\TimeBundle\Twig\Extension\TimeExtension')->diff($this->getAttribute($context["comment"], "created", array()));
            echo "</span>
                                </small>
                            </div>
                        </li>
                       
                    </ul>
                </div>
                ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 253
            echo "                <div class=\"col-md-12\" >
                    <div class=\"card\"  style=\"margin-top: 0px;\">
                        <div class=\"card-content\">
                            <center><img src=\"";
            // line 256
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/bg_empty.png"), "html", null, true);
            echo "\"  style=\"width: 100% !important;\" =\"\"></center>
                            <br>
                        </div>
                    </div>
                </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['comment'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 262
        echo "            </div>
        </div>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "AppBundle:Wallpaper:view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  600 => 262,  588 => 256,  583 => 253,  570 => 245,  564 => 242,  559 => 239,  552 => 235,  549 => 234,  541 => 231,  539 => 230,  533 => 227,  527 => 224,  523 => 223,  516 => 219,  510 => 215,  505 => 214,  493 => 205,  488 => 203,  483 => 201,  479 => 200,  475 => 199,  471 => 198,  467 => 197,  459 => 192,  454 => 190,  449 => 188,  445 => 187,  441 => 186,  437 => 185,  433 => 184,  424 => 178,  419 => 176,  414 => 174,  410 => 173,  406 => 172,  402 => 171,  398 => 170,  390 => 165,  385 => 163,  380 => 161,  376 => 160,  372 => 159,  368 => 158,  364 => 157,  356 => 152,  351 => 150,  346 => 148,  342 => 147,  338 => 146,  334 => 145,  330 => 144,  323 => 139,  317 => 138,  314 => 137,  308 => 135,  305 => 134,  299 => 132,  296 => 131,  290 => 129,  287 => 128,  281 => 126,  278 => 125,  274 => 124,  265 => 118,  260 => 115,  257 => 114,  255 => 113,  242 => 103,  238 => 102,  234 => 101,  230 => 100,  219 => 92,  211 => 88,  205 => 86,  199 => 84,  197 => 83,  186 => 74,  175 => 71,  170 => 70,  166 => 69,  157 => 62,  148 => 59,  145 => 58,  141 => 57,  131 => 49,  127 => 48,  123 => 46,  121 => 45,  116 => 42,  112 => 41,  108 => 39,  106 => 38,  101 => 35,  97 => 34,  93 => 32,  91 => 31,  87 => 29,  81 => 27,  78 => 26,  69 => 21,  67 => 20,  63 => 19,  58 => 17,  54 => 15,  50 => 13,  48 => 12,  45 => 11,  43 => 10,  40 => 9,  38 => 8,  31 => 3,  28 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "AppBundle:Wallpaper:view.html.twig", "/home/kaman/projects/Web/src/AppBundle/Resources/views/Wallpaper/view.html.twig");
    }
}
