<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Settings
 *
 * @ORM\Table(name="settings_table")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SettingsRepository")
 */
class Settings
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="firebasekey", type="text")
     */
    private $firebasekey;

   
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
    * Get firebasekey
    * @return  
    */
    public function getFirebasekey()
    {
        return $this->firebasekey;
    }
    
    /**
    * Set firebasekey
    * @return $this
    */
    public function setFirebasekey($firebasekey)
    {
        $this->firebasekey = $firebasekey;
        return $this;
    }
   
}
